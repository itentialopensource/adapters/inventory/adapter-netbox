# NetBox

Vendor: NetBox
Homepage: https://netboxlabs.com/

Product: NetBox
Product Page: https://netboxlabs.com/oss/netbox/

## Introduction
We classify NetBox into the Inventory domain because it contains an inventory of devices, assets, network discovery and topology information.

"NetBox provides the ideal "source of truth" to power network automation."

## Why Integrate
The NetBox adapter from Itential is used to integrate the Itential Automation Platform (IAP) with NetBox. With this adapter you have the ability to perform operations such as:

- Add and Remove Inventory Items.
- Create Subnet
- Create IP Record
- Get Next Available IP
- Get Circuit
- Get L2VPN
- Get Device

## Additional Product Documentation
The [API documents for Netbox](https://demo.netbox.dev/)