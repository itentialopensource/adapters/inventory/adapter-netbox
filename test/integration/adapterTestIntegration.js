/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-netbox',
      type: 'Netbox',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Netbox = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Netbox Adapter Test', () => {
  describe('Netbox Class Tests', () => {
    const a = new Netbox(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    let circuitsId = 'fakedata';
    const circuitsPostCircuitsCircuitTerminationsBodyParam = {
      circuit: 7,
      term_side: 'Z',
      site: 8,
      port_speed: 8
    };
    describe('#postCircuitsCircuitTerminations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCircuitsCircuitTerminations(circuitsPostCircuitsCircuitTerminationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('object', typeof data.response.circuit);
                assert.equal('Z', data.response.termSide);
                assert.equal('object', typeof data.response.site);
                assert.equal(3, data.response.portSpeed);
                assert.equal(5, data.response.upstreamSpeed);
                assert.equal('string', data.response.xconnectId);
                assert.equal('string', data.response.pp_info);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connection_status);
                assert.equal('object', typeof data.response.cable);
              } else {
                runCommonAsserts(data, error);
              }
              circuitsId = data.response.id;
              saveMockData('Circuits', 'postCircuitsCircuitTerminations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPostCircuitsCircuitTypesBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postCircuitsCircuitTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCircuitsCircuitTypes(circuitsPostCircuitsCircuitTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.circuit_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'postCircuitsCircuitTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPostCircuitsCircuitsBodyParam = {
      cid: 'string',
      provider: 9,
      type: 7
    };
    describe('#postCircuitsCircuits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCircuitsCircuits(circuitsPostCircuitsCircuitsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.cid);
                assert.equal('object', typeof data.response.provider);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('string', data.response.installDate);
                assert.equal(8, data.response.commitRate);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.termination_a);
                assert.equal('object', typeof data.response.termination_z);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'postCircuitsCircuits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPostCircuitsProvidersBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postCircuitsProviders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCircuitsProviders(circuitsPostCircuitsProvidersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal(7, data.response.asn);
                assert.equal('string', data.response.account);
                assert.equal('string', data.response.portal_url);
                assert.equal('string', data.response.noc_contact);
                assert.equal('string', data.response.admin_contact);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(1, data.response.circuit_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'postCircuitsProviders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTerminations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCircuitsCircuitTerminations(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'getCircuitsCircuitTerminations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPutCircuitsCircuitTerminationsIdBodyParam = {
      circuit: 7,
      term_side: 'A',
      site: 2,
      port_speed: 1
    };
    describe('#putCircuitsCircuitTerminationsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCircuitsCircuitTerminationsId(circuitsId, circuitsPutCircuitsCircuitTerminationsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'putCircuitsCircuitTerminationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPatchCircuitsCircuitTerminationsIdBodyParam = {
      circuit: 6,
      term_side: 'Z',
      site: 6,
      port_speed: 2
    };
    describe('#patchCircuitsCircuitTerminationsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchCircuitsCircuitTerminationsId(circuitsId, circuitsPatchCircuitsCircuitTerminationsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'patchCircuitsCircuitTerminationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTerminationsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCircuitsCircuitTerminationsId(circuitsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('object', typeof data.response.circuit);
                assert.equal('A', data.response.termSide);
                assert.equal('object', typeof data.response.site);
                assert.equal(8, data.response.portSpeed);
                assert.equal(5, data.response.upstreamSpeed);
                assert.equal('string', data.response.xconnectId);
                assert.equal('string', data.response.pp_info);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connection_status);
                assert.equal('object', typeof data.response.cable);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'getCircuitsCircuitTerminationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCircuitsCircuitTypes(circuitsId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'getCircuitsCircuitTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPutCircuitsCircuitTypesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putCircuitsCircuitTypesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCircuitsCircuitTypesId(circuitsId, circuitsPutCircuitsCircuitTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'putCircuitsCircuitTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPatchCircuitsCircuitTypesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchCircuitsCircuitTypesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchCircuitsCircuitTypesId(circuitsId, circuitsPatchCircuitsCircuitTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'patchCircuitsCircuitTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitTypesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCircuitsCircuitTypesId(circuitsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.circuit_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'getCircuitsCircuitTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCircuitsCircuits(circuitsId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'getCircuitsCircuits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPutCircuitsCircuitsIdBodyParam = {
      cid: 'string',
      provider: 7,
      type: 9
    };
    describe('#putCircuitsCircuitsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCircuitsCircuitsId(circuitsId, circuitsPutCircuitsCircuitsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'putCircuitsCircuitsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPatchCircuitsCircuitsIdBodyParam = {
      cid: 'string',
      provider: 7,
      type: 5
    };
    describe('#patchCircuitsCircuitsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchCircuitsCircuitsId(circuitsId, circuitsPatchCircuitsCircuitsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'patchCircuitsCircuitsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsCircuitsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCircuitsCircuitsId(circuitsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.cid);
                assert.equal('object', typeof data.response.provider);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('string', data.response.installDate);
                assert.equal(3, data.response.commitRate);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.termination_a);
                assert.equal('object', typeof data.response.termination_z);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'getCircuitsCircuitsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsProviders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCircuitsProviders(circuitsId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'getCircuitsProviders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPutCircuitsProvidersIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putCircuitsProvidersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putCircuitsProvidersId(circuitsId, circuitsPutCircuitsProvidersIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'putCircuitsProvidersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const circuitsPatchCircuitsProvidersIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchCircuitsProvidersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchCircuitsProvidersId(circuitsId, circuitsPatchCircuitsProvidersIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'patchCircuitsProvidersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsProvidersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCircuitsProvidersId(circuitsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal(4, data.response.asn);
                assert.equal('string', data.response.account);
                assert.equal('string', data.response.portal_url);
                assert.equal('string', data.response.noc_contact);
                assert.equal('string', data.response.admin_contact);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(10, data.response.circuit_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'getCircuitsProvidersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCircuitsProvidersIdGraphs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getCircuitsProvidersIdGraphs(circuitsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal(8, data.response.asn);
                assert.equal('string', data.response.account);
                assert.equal('string', data.response.portal_url);
                assert.equal('string', data.response.noc_contact);
                assert.equal('string', data.response.admin_contact);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(4, data.response.circuit_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'getCircuitsProvidersIdGraphs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let dcimId = 'fakedata';
    const dcimPostDcimCablesBodyParam = {
      termination_a_type: 'string',
      termination_a_id: 4,
      termination_b_type: 'string',
      termination_b_id: 3
    };
    describe('#postDcimCables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimCables(dcimPostDcimCablesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.termination_a_type);
                assert.equal(1, data.response.termination_a_id);
                assert.equal('object', typeof data.response.termination_a);
                assert.equal('string', data.response.termination_b_type);
                assert.equal(1, data.response.termination_b_id);
                assert.equal('object', typeof data.response.termination_b);
                assert.equal('smf-os2', data.response.type);
                assert.equal('object', typeof data.response.status);
                assert.equal('string', data.response.label);
                assert.equal('string', data.response.color);
                assert.equal(5, data.response.length);
                assert.equal('object', typeof data.response.lengthUnit);
              } else {
                runCommonAsserts(data, error);
              }
              dcimId = data.response.id;
              saveMockData('Dcim', 'postDcimCables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimConsolePortTemplatesBodyParam = {
      device_type: 2,
      name: 'string'
    };
    describe('#postDcimConsolePortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimConsolePortTemplates(dcimPostDcimConsolePortTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimConsolePortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimConsolePortsBodyParam = {
      device: 5,
      name: 'string'
    };
    describe('#postDcimConsolePorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimConsolePorts(dcimPostDcimConsolePortsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimConsolePorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimConsoleServerPortTemplatesBodyParam = {
      device_type: 3,
      name: 'string'
    };
    describe('#postDcimConsoleServerPortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimConsoleServerPortTemplates(dcimPostDcimConsoleServerPortTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimConsoleServerPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimConsoleServerPortsBodyParam = {
      device: 3,
      name: 'string'
    };
    describe('#postDcimConsoleServerPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimConsoleServerPorts(dcimPostDcimConsoleServerPortsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimConsoleServerPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimDeviceBayTemplatesBodyParam = {
      device_type: 8,
      name: 'string'
    };
    describe('#postDcimDeviceBayTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimDeviceBayTemplates(dcimPostDcimDeviceBayTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimDeviceBayTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimDeviceBaysBodyParam = {
      device: 4,
      name: 'string'
    };
    describe('#postDcimDeviceBays - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimDeviceBays(dcimPostDcimDeviceBaysBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.installed_device);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimDeviceBays', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimDeviceRolesBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postDcimDeviceRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimDeviceRoles(dcimPostDcimDeviceRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.color);
                assert.equal(false, data.response.vmRole);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.device_count);
                assert.equal(8, data.response.virtualmachine_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimDeviceRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimDeviceTypesBodyParam = {
      manufacturer: 6,
      model: 'string',
      slug: 'string'
    };
    describe('#postDcimDeviceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimDeviceTypes(dcimPostDcimDeviceTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('object', typeof data.response.manufacturer);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.display_name);
                assert.equal('string', data.response.partNumber);
                assert.equal(3, data.response.uHeight);
                assert.equal(true, data.response.isFullDepth);
                assert.equal('object', typeof data.response.subdeviceRole);
                assert.equal('string', data.response.front_image);
                assert.equal('string', data.response.rear_image);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(8, data.response.device_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimDeviceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimDevicesBodyParam = {
      device_type: 3,
      device_role: 8,
      site: 7
    };
    describe('#postDcimDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimDevices(dcimPostDcimDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('object', typeof data.response.device_role);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.platform);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.assetTag);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.rack);
                assert.equal(6, data.response.position);
                assert.equal('object', typeof data.response.face);
                assert.equal('object', typeof data.response.parent_device);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.primary_ip);
                assert.equal('object', typeof data.response.primary_ip4);
                assert.equal('object', typeof data.response.primary_ip6);
                assert.equal('object', typeof data.response.cluster);
                assert.equal('object', typeof data.response.virtual_chassis);
                assert.equal(10, data.response.vcPosition);
                assert.equal(9, data.response.vcPriority);
                assert.equal('string', data.response.comments);
                assert.equal('string', data.response.localContextData);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('object', typeof data.response.config_context);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimFrontPortTemplatesBodyParam = {
      device_type: 10,
      name: 'string',
      type: '110-punch',
      rear_port: 7
    };
    describe('#postDcimFrontPortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimFrontPortTemplates(dcimPostDcimFrontPortTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.rear_port);
                assert.equal(1, data.response.rear_port_position);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimFrontPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimFrontPortsBodyParam = {
      device: 1,
      name: 'string',
      type: '8p8c',
      rear_port: 10
    };
    describe('#postDcimFrontPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimFrontPorts(dcimPostDcimFrontPortsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.rear_port);
                assert.equal(1, data.response.rear_port_position);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimFrontPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimInterfaceTemplatesBodyParam = {
      device_type: 4,
      name: 'string',
      type: 'other'
    };
    describe('#postDcimInterfaceTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimInterfaceTemplates(dcimPostDcimInterfaceTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(false, data.response.mgmtOnly);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimInterfaceTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimInterfacesBodyParam = {
      device: 6,
      name: 'string',
      type: '1000base-x-sfp'
    };
    describe('#postDcimInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimInterfaces(dcimPostDcimInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(false, data.response.enabled);
                assert.equal('object', typeof data.response.lag);
                assert.equal(3, data.response.mtu);
                assert.equal('string', data.response.macAddress);
                assert.equal(true, data.response.mgmtOnly);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal('object', typeof data.response.mode);
                assert.equal('object', typeof data.response.untagged_vlan);
                assert.equal(true, Array.isArray(data.response.tagged_vlans));
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(3, data.response.count_ipaddresses);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimInventoryItemsBodyParam = {
      device: 8,
      name: 'string'
    };
    describe('#postDcimInventoryItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimInventoryItems(dcimPostDcimInventoryItemsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal(5, data.response.parent);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.manufacturer);
                assert.equal('string', data.response.partId);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.assetTag);
                assert.equal(false, data.response.discovered);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimInventoryItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimManufacturersBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postDcimManufacturers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimManufacturers(dcimPostDcimManufacturersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(8, data.response.devicetype_count);
                assert.equal(5, data.response.inventoryitem_count);
                assert.equal(3, data.response.platform_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimManufacturers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimPlatformsBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postDcimPlatforms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimPlatforms(dcimPostDcimPlatformsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.manufacturer);
                assert.equal('string', data.response.napalmDriver);
                assert.equal('string', data.response.napalm_args);
                assert.equal('string', data.response.description);
                assert.equal(5, data.response.device_count);
                assert.equal(4, data.response.virtualmachine_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimPowerFeedsBodyParam = {
      power_panel: 6,
      name: 'string'
    };
    describe('#postDcimPowerFeeds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimPowerFeeds(dcimPostDcimPowerFeedsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('object', typeof data.response.power_panel);
                assert.equal('object', typeof data.response.rack);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.supply);
                assert.equal('object', typeof data.response.phase);
                assert.equal(5, data.response.voltage);
                assert.equal(9, data.response.amperage);
                assert.equal(8, data.response.maxUtilization);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimPowerFeeds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimPowerOutletTemplatesBodyParam = {
      device_type: 4,
      name: 'string'
    };
    describe('#postDcimPowerOutletTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimPowerOutletTemplates(dcimPostDcimPowerOutletTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.power_port);
                assert.equal('object', typeof data.response.feedLeg);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimPowerOutletTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimPowerOutletsBodyParam = {
      device: 4,
      name: 'string'
    };
    describe('#postDcimPowerOutlets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimPowerOutlets(dcimPostDcimPowerOutletsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.power_port);
                assert.equal('object', typeof data.response.feedLeg);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimPowerOutlets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimPowerPanelsBodyParam = {
      site: 1,
      name: 'string'
    };
    describe('#postDcimPowerPanels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimPowerPanels(dcimPostDcimPowerPanelsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.rack_group);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.powerfeed_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimPowerPanels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimPowerPortTemplatesBodyParam = {
      device_type: 2,
      name: 'string'
    };
    describe('#postDcimPowerPortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimPowerPortTemplates(dcimPostDcimPowerPortTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(8, data.response.maximumDraw);
                assert.equal(7, data.response.allocatedDraw);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimPowerPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimPowerPortsBodyParam = {
      device: 2,
      name: 'string'
    };
    describe('#postDcimPowerPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimPowerPorts(dcimPostDcimPowerPortsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(4, data.response.maximumDraw);
                assert.equal(7, data.response.allocatedDraw);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimPowerPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimRackGroupsBodyParam = {
      name: 'string',
      slug: 'string',
      site: 2
    };
    describe('#postDcimRackGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimRackGroups(dcimPostDcimRackGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.description);
                assert.equal(7, data.response.rack_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimRackGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimRackReservationsBodyParam = {
      rack: 1,
      units: [
        3
      ],
      user: 10,
      description: 'string'
    };
    describe('#postDcimRackReservations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimRackReservations(dcimPostDcimRackReservationsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('object', typeof data.response.rack);
                assert.equal(true, Array.isArray(data.response.units));
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.user);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('string', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimRackReservations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimRackRolesBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postDcimRackRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimRackRoles(dcimPostDcimRackRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.rack_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimRackRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimRacksBodyParam = {
      name: 'string',
      site: 1
    };
    describe('#postDcimRacks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimRacks(dcimPostDcimRacksBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.facilityId);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.group);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.role);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.assetTag);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.width);
                assert.equal(5, data.response.uHeight);
                assert.equal(true, data.response.descUnits);
                assert.equal(1, data.response.outerWidth);
                assert.equal(10, data.response.outerDepth);
                assert.equal('object', typeof data.response.outerUnit);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(2, data.response.device_count);
                assert.equal(8, data.response.powerfeed_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimRacks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimRearPortTemplatesBodyParam = {
      device_type: 5,
      name: 'string',
      type: 'lc'
    };
    describe('#postDcimRearPortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimRearPortTemplates(dcimPostDcimRearPortTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(3, data.response.positions);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimRearPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimRearPortsBodyParam = {
      device: 7,
      name: 'string',
      type: 'lsh-apc'
    };
    describe('#postDcimRearPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimRearPorts(dcimPostDcimRearPortsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(10, data.response.positions);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimRearPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimRegionsBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postDcimRegions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimRegions(dcimPostDcimRegionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.site_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimRegions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimSitesBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postDcimSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimSites(dcimPostDcimSitesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('string', data.response.facility);
                assert.equal(10, data.response.asn);
                assert.equal('string', data.response.time_zone);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.physical_address);
                assert.equal('string', data.response.shipping_address);
                assert.equal('string', data.response.latitude);
                assert.equal('string', data.response.longitude);
                assert.equal('string', data.response.contactName);
                assert.equal('string', data.response.contactPhone);
                assert.equal('string', data.response.contactEmail);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(3, data.response.circuit_count);
                assert.equal(8, data.response.device_count);
                assert.equal(1, data.response.prefix_count);
                assert.equal(8, data.response.rack_count);
                assert.equal(3, data.response.virtualmachine_count);
                assert.equal(9, data.response.vlan_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimVirtualChassisBodyParam = {
      master: 8
    };
    describe('#postDcimVirtualChassis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDcimVirtualChassis(dcimPostDcimVirtualChassisBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('object', typeof data.response.master);
                assert.equal('string', data.response.domain);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(2, data.response.member_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimVirtualChassis', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimCables - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimCables(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimCables', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimCablesIdBodyParam = {
      termination_a_type: 'string',
      termination_a_id: 2,
      termination_b_type: 'string',
      termination_b_id: 4
    };
    describe('#putDcimCablesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimCablesId(dcimId, dcimPutDcimCablesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimCablesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimCablesIdBodyParam = {
      termination_a_type: 'string',
      termination_a_id: 6,
      termination_b_type: 'string',
      termination_b_id: 6
    };
    describe('#patchDcimCablesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimCablesId(dcimId, dcimPatchDcimCablesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimCablesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimCablesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimCablesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.termination_a_type);
                assert.equal(9, data.response.termination_a_id);
                assert.equal('object', typeof data.response.termination_a);
                assert.equal('string', data.response.termination_b_type);
                assert.equal(8, data.response.termination_b_id);
                assert.equal('object', typeof data.response.termination_b);
                assert.equal('dac-passive', data.response.type);
                assert.equal('object', typeof data.response.status);
                assert.equal('string', data.response.label);
                assert.equal('string', data.response.color);
                assert.equal(8, data.response.length);
                assert.equal('object', typeof data.response.lengthUnit);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimCablesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPeerDevice = 'fakedata';
    const dcimPeerInterface = 'fakedata';
    describe('#getDcimConnectedDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConnectedDevice(dcimPeerDevice, dcimPeerInterface, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('object', typeof data.response.device_role);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.platform);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.assetTag);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.rack);
                assert.equal(8, data.response.position);
                assert.equal('object', typeof data.response.face);
                assert.equal('object', typeof data.response.parent_device);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.primary_ip);
                assert.equal('object', typeof data.response.primary_ip4);
                assert.equal('object', typeof data.response.primary_ip6);
                assert.equal('object', typeof data.response.cluster);
                assert.equal('object', typeof data.response.virtual_chassis);
                assert.equal(5, data.response.vcPosition);
                assert.equal(4, data.response.vcPriority);
                assert.equal('string', data.response.comments);
                assert.equal('string', data.response.localContextData);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConnectedDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsoleConnections(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsoleConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsolePortTemplates(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsolePortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimConsolePortTemplatesIdBodyParam = {
      device_type: 7,
      name: 'string'
    };
    describe('#putDcimConsolePortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimConsolePortTemplatesId(dcimId, dcimPutDcimConsolePortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimConsolePortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimConsolePortTemplatesIdBodyParam = {
      device_type: 6,
      name: 'string'
    };
    describe('#patchDcimConsolePortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimConsolePortTemplatesId(dcimId, dcimPatchDcimConsolePortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimConsolePortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsolePortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsolePortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsolePorts(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsolePorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimConsolePortsIdBodyParam = {
      device: 8,
      name: 'string'
    };
    describe('#putDcimConsolePortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimConsolePortsId(dcimId, dcimPutDcimConsolePortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimConsolePortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimConsolePortsIdBodyParam = {
      device: 5,
      name: 'string'
    };
    describe('#patchDcimConsolePortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimConsolePortsId(dcimId, dcimPatchDcimConsolePortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimConsolePortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsolePortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsolePortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsolePortsIdTrace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsolePortsIdTrace(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsolePortsIdTrace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsoleServerPortTemplates(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsoleServerPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimConsoleServerPortTemplatesIdBodyParam = {
      device_type: 8,
      name: 'string'
    };
    describe('#putDcimConsoleServerPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimConsoleServerPortTemplatesId(dcimId, dcimPutDcimConsoleServerPortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimConsoleServerPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimConsoleServerPortTemplatesIdBodyParam = {
      device_type: 2,
      name: 'string'
    };
    describe('#patchDcimConsoleServerPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimConsoleServerPortTemplatesId(dcimId, dcimPatchDcimConsoleServerPortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimConsoleServerPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsoleServerPortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsoleServerPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsoleServerPorts(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsoleServerPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimConsoleServerPortsIdBodyParam = {
      device: 1,
      name: 'string'
    };
    describe('#putDcimConsoleServerPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimConsoleServerPortsId(dcimId, dcimPutDcimConsoleServerPortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimConsoleServerPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimConsoleServerPortsIdBodyParam = {
      device: 2,
      name: 'string'
    };
    describe('#patchDcimConsoleServerPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimConsoleServerPortsId(dcimId, dcimPatchDcimConsoleServerPortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimConsoleServerPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsoleServerPortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsoleServerPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimConsoleServerPortsIdTrace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimConsoleServerPortsIdTrace(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimConsoleServerPortsIdTrace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBayTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDeviceBayTemplates(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDeviceBayTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimDeviceBayTemplatesIdBodyParam = {
      device_type: 2,
      name: 'string'
    };
    describe('#putDcimDeviceBayTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimDeviceBayTemplatesId(dcimId, dcimPutDcimDeviceBayTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimDeviceBayTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimDeviceBayTemplatesIdBodyParam = {
      device_type: 4,
      name: 'string'
    };
    describe('#patchDcimDeviceBayTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimDeviceBayTemplatesId(dcimId, dcimPatchDcimDeviceBayTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimDeviceBayTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBayTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDeviceBayTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDeviceBayTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBays - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDeviceBays(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDeviceBays', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimDeviceBaysIdBodyParam = {
      device: 10,
      name: 'string'
    };
    describe('#putDcimDeviceBaysId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimDeviceBaysId(dcimId, dcimPutDcimDeviceBaysIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimDeviceBaysId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimDeviceBaysIdBodyParam = {
      device: 2,
      name: 'string'
    };
    describe('#patchDcimDeviceBaysId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimDeviceBaysId(dcimId, dcimPatchDcimDeviceBaysIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimDeviceBaysId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceBaysId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDeviceBaysId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.installed_device);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDeviceBaysId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDeviceRoles(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDeviceRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimDeviceRolesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putDcimDeviceRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimDeviceRolesId(dcimId, dcimPutDcimDeviceRolesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimDeviceRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimDeviceRolesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchDcimDeviceRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimDeviceRolesId(dcimId, dcimPatchDcimDeviceRolesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimDeviceRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDeviceRolesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.color);
                assert.equal(true, data.response.vmRole);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.device_count);
                assert.equal(1, data.response.virtualmachine_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDeviceRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDeviceTypes(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDeviceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimDeviceTypesIdBodyParam = {
      manufacturer: 5,
      model: 'string',
      slug: 'string'
    };
    describe('#putDcimDeviceTypesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimDeviceTypesId(dcimId, dcimPutDcimDeviceTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimDeviceTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimDeviceTypesIdBodyParam = {
      manufacturer: 3,
      model: 'string',
      slug: 'string'
    };
    describe('#patchDcimDeviceTypesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimDeviceTypesId(dcimId, dcimPatchDcimDeviceTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimDeviceTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDeviceTypesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDeviceTypesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('object', typeof data.response.manufacturer);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.display_name);
                assert.equal('string', data.response.partNumber);
                assert.equal(3, data.response.uHeight);
                assert.equal(false, data.response.isFullDepth);
                assert.equal('object', typeof data.response.subdeviceRole);
                assert.equal('string', data.response.front_image);
                assert.equal('string', data.response.rear_image);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(2, data.response.device_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDeviceTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDevices(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimDevicesIdBodyParam = {
      device_type: 8,
      device_role: 1,
      site: 8
    };
    describe('#putDcimDevicesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimDevicesId(dcimId, dcimPutDcimDevicesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimDevicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimDevicesIdBodyParam = {
      device_type: 3,
      device_role: 8,
      site: 7
    };
    describe('#patchDcimDevicesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimDevicesId(dcimId, dcimPatchDcimDevicesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimDevicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevicesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDevicesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('object', typeof data.response.device_role);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.platform);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.assetTag);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.rack);
                assert.equal(5, data.response.position);
                assert.equal('object', typeof data.response.face);
                assert.equal('object', typeof data.response.parent_device);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.primary_ip);
                assert.equal('object', typeof data.response.primary_ip4);
                assert.equal('object', typeof data.response.primary_ip6);
                assert.equal('object', typeof data.response.cluster);
                assert.equal('object', typeof data.response.virtual_chassis);
                assert.equal(10, data.response.vcPosition);
                assert.equal(7, data.response.vcPriority);
                assert.equal('string', data.response.comments);
                assert.equal('string', data.response.localContextData);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('object', typeof data.response.config_context);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDevicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevicesIdGraphs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDevicesIdGraphs(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('object', typeof data.response.device_role);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.platform);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.assetTag);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.rack);
                assert.equal(2, data.response.position);
                assert.equal('object', typeof data.response.face);
                assert.equal('object', typeof data.response.parent_device);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.primary_ip);
                assert.equal('object', typeof data.response.primary_ip4);
                assert.equal('object', typeof data.response.primary_ip6);
                assert.equal('object', typeof data.response.cluster);
                assert.equal('object', typeof data.response.virtual_chassis);
                assert.equal(10, data.response.vcPosition);
                assert.equal(2, data.response.vcPriority);
                assert.equal('string', data.response.comments);
                assert.equal('string', data.response.localContextData);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('object', typeof data.response.config_context);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDevicesIdGraphs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimDevicesIdNapalm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimDevicesIdNapalm(dcimId, 'fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.method);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimDevicesIdNapalm', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimFrontPortTemplates(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimFrontPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimFrontPortTemplatesIdBodyParam = {
      device_type: 5,
      name: 'string',
      type: 'sc-apc',
      rear_port: 8
    };
    describe('#putDcimFrontPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimFrontPortTemplatesId(dcimId, dcimPutDcimFrontPortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimFrontPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimFrontPortTemplatesIdBodyParam = {
      device_type: 10,
      name: 'string',
      type: 'mpo',
      rear_port: 1
    };
    describe('#patchDcimFrontPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimFrontPortTemplatesId(dcimId, dcimPatchDcimFrontPortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimFrontPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimFrontPortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.rear_port);
                assert.equal(1, data.response.rear_port_position);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimFrontPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimFrontPorts(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimFrontPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimFrontPortsIdBodyParam = {
      device: 2,
      name: 'string',
      type: 'fc',
      rear_port: 2
    };
    describe('#putDcimFrontPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimFrontPortsId(dcimId, dcimPutDcimFrontPortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimFrontPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimFrontPortsIdBodyParam = {
      device: 5,
      name: 'string',
      type: 'lsh',
      rear_port: 3
    };
    describe('#patchDcimFrontPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimFrontPortsId(dcimId, dcimPatchDcimFrontPortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimFrontPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimFrontPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimFrontPortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.rear_port);
                assert.equal(1, data.response.rear_port_position);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimFrontPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaceConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimInterfaceConnections(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimInterfaceConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaceTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimInterfaceTemplates(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimInterfaceTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimInterfaceTemplatesIdBodyParam = {
      device_type: 9,
      name: 'string',
      type: 'sonet-oc12'
    };
    describe('#putDcimInterfaceTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimInterfaceTemplatesId(dcimId, dcimPutDcimInterfaceTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimInterfaceTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimInterfaceTemplatesIdBodyParam = {
      device_type: 7,
      name: 'string',
      type: 'sonet-oc48'
    };
    describe('#patchDcimInterfaceTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimInterfaceTemplatesId(dcimId, dcimPatchDcimInterfaceTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimInterfaceTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaceTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimInterfaceTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(true, data.response.mgmtOnly);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimInterfaceTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimInterfaces(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimInterfacesIdBodyParam = {
      device: 1,
      name: 'string',
      type: '1gfc-sfp'
    };
    describe('#putDcimInterfacesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimInterfacesId(dcimId, dcimPutDcimInterfacesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimInterfacesIdBodyParam = {
      device: 3,
      name: 'string',
      type: 'infiniband-ddr'
    };
    describe('#patchDcimInterfacesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimInterfacesId(dcimId, dcimPatchDcimInterfacesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfacesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimInterfacesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.lag);
                assert.equal(4, data.response.mtu);
                assert.equal('string', data.response.macAddress);
                assert.equal(true, data.response.mgmtOnly);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal('object', typeof data.response.mode);
                assert.equal('object', typeof data.response.untagged_vlan);
                assert.equal(true, Array.isArray(data.response.tagged_vlans));
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(1, data.response.count_ipaddresses);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfacesIdGraphs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimInterfacesIdGraphs(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.lag);
                assert.equal(4, data.response.mtu);
                assert.equal('string', data.response.macAddress);
                assert.equal(false, data.response.mgmtOnly);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal('object', typeof data.response.mode);
                assert.equal('object', typeof data.response.untagged_vlan);
                assert.equal(true, Array.isArray(data.response.tagged_vlans));
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(7, data.response.count_ipaddresses);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimInterfacesIdGraphs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInterfacesIdTrace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimInterfacesIdTrace(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.lag);
                assert.equal(7, data.response.mtu);
                assert.equal('string', data.response.macAddress);
                assert.equal(false, data.response.mgmtOnly);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal('object', typeof data.response.mode);
                assert.equal('object', typeof data.response.untagged_vlan);
                assert.equal(true, Array.isArray(data.response.tagged_vlans));
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(5, data.response.count_ipaddresses);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimInterfacesIdTrace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInventoryItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimInventoryItems(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimInventoryItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimInventoryItemsIdBodyParam = {
      device: 8,
      name: 'string'
    };
    describe('#putDcimInventoryItemsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimInventoryItemsId(dcimId, dcimPutDcimInventoryItemsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimInventoryItemsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimInventoryItemsIdBodyParam = {
      device: 1,
      name: 'string'
    };
    describe('#patchDcimInventoryItemsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimInventoryItemsId(dcimId, dcimPatchDcimInventoryItemsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimInventoryItemsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimInventoryItemsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimInventoryItemsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal(3, data.response.parent);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.manufacturer);
                assert.equal('string', data.response.partId);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.assetTag);
                assert.equal(true, data.response.discovered);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimInventoryItemsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimManufacturers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimManufacturers(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimManufacturers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimManufacturersIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putDcimManufacturersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimManufacturersId(dcimId, dcimPutDcimManufacturersIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimManufacturersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimManufacturersIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchDcimManufacturersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimManufacturersId(dcimId, dcimPatchDcimManufacturersIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimManufacturersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimManufacturersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimManufacturersId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(8, data.response.devicetype_count);
                assert.equal(7, data.response.inventoryitem_count);
                assert.equal(7, data.response.platform_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimManufacturersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPlatforms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPlatforms(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimPlatformsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putDcimPlatformsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimPlatformsId(dcimId, dcimPutDcimPlatformsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimPlatformsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimPlatformsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchDcimPlatformsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimPlatformsId(dcimId, dcimPatchDcimPlatformsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimPlatformsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPlatformsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPlatformsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.manufacturer);
                assert.equal('string', data.response.napalmDriver);
                assert.equal('string', data.response.napalm_args);
                assert.equal('string', data.response.description);
                assert.equal(8, data.response.device_count);
                assert.equal(4, data.response.virtualmachine_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPlatformsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerConnections(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerFeeds - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerFeeds(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerFeeds', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimPowerFeedsIdBodyParam = {
      power_panel: 10,
      name: 'string'
    };
    describe('#putDcimPowerFeedsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimPowerFeedsId(dcimId, dcimPutDcimPowerFeedsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimPowerFeedsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimPowerFeedsIdBodyParam = {
      power_panel: 4,
      name: 'string'
    };
    describe('#patchDcimPowerFeedsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimPowerFeedsId(dcimId, dcimPatchDcimPowerFeedsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimPowerFeedsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerFeedsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerFeedsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('object', typeof data.response.power_panel);
                assert.equal('object', typeof data.response.rack);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.supply);
                assert.equal('object', typeof data.response.phase);
                assert.equal(1, data.response.voltage);
                assert.equal(2, data.response.amperage);
                assert.equal(9, data.response.maxUtilization);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerFeedsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerOutletTemplates(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerOutletTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimPowerOutletTemplatesIdBodyParam = {
      device_type: 9,
      name: 'string'
    };
    describe('#putDcimPowerOutletTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimPowerOutletTemplatesId(dcimId, dcimPutDcimPowerOutletTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimPowerOutletTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimPowerOutletTemplatesIdBodyParam = {
      device_type: 6,
      name: 'string'
    };
    describe('#patchDcimPowerOutletTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimPowerOutletTemplatesId(dcimId, dcimPatchDcimPowerOutletTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimPowerOutletTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerOutletTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.power_port);
                assert.equal('object', typeof data.response.feedLeg);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerOutletTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutlets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerOutlets(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerOutlets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimPowerOutletsIdBodyParam = {
      device: 2,
      name: 'string'
    };
    describe('#putDcimPowerOutletsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimPowerOutletsId(dcimId, dcimPutDcimPowerOutletsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimPowerOutletsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimPowerOutletsIdBodyParam = {
      device: 8,
      name: 'string'
    };
    describe('#patchDcimPowerOutletsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimPowerOutletsId(dcimId, dcimPatchDcimPowerOutletsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimPowerOutletsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerOutletsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.power_port);
                assert.equal('object', typeof data.response.feedLeg);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerOutletsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerOutletsIdTrace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerOutletsIdTrace(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.power_port);
                assert.equal('object', typeof data.response.feedLeg);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerOutletsIdTrace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPanels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerPanels(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerPanels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimPowerPanelsIdBodyParam = {
      site: 6,
      name: 'string'
    };
    describe('#putDcimPowerPanelsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimPowerPanelsId(dcimId, dcimPutDcimPowerPanelsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimPowerPanelsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimPowerPanelsIdBodyParam = {
      site: 9,
      name: 'string'
    };
    describe('#patchDcimPowerPanelsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimPowerPanelsId(dcimId, dcimPatchDcimPowerPanelsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimPowerPanelsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPanelsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerPanelsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.rack_group);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.powerfeed_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerPanelsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerPortTemplates(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimPowerPortTemplatesIdBodyParam = {
      device_type: 1,
      name: 'string'
    };
    describe('#putDcimPowerPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimPowerPortTemplatesId(dcimId, dcimPutDcimPowerPortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimPowerPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimPowerPortTemplatesIdBodyParam = {
      device_type: 9,
      name: 'string'
    };
    describe('#patchDcimPowerPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimPowerPortTemplatesId(dcimId, dcimPatchDcimPowerPortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimPowerPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerPortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(4, data.response.maximumDraw);
                assert.equal(8, data.response.allocatedDraw);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerPorts(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimPowerPortsIdBodyParam = {
      device: 10,
      name: 'string'
    };
    describe('#putDcimPowerPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimPowerPortsId(dcimId, dcimPutDcimPowerPortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimPowerPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimPowerPortsIdBodyParam = {
      device: 2,
      name: 'string'
    };
    describe('#patchDcimPowerPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimPowerPortsId(dcimId, dcimPatchDcimPowerPortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimPowerPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerPortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(2, data.response.maximumDraw);
                assert.equal(9, data.response.allocatedDraw);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimPowerPortsIdTrace - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimPowerPortsIdTrace(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(5, data.response.maximumDraw);
                assert.equal(2, data.response.allocatedDraw);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.connected_endpoint_type);
                assert.equal('object', typeof data.response.connected_endpoint);
                assert.equal('object', typeof data.response.connectionStatus);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimPowerPortsIdTrace', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRackGroups(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRackGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimRackGroupsIdBodyParam = {
      name: 'string',
      slug: 'string',
      site: 3
    };
    describe('#putDcimRackGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimRackGroupsId(dcimId, dcimPutDcimRackGroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimRackGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimRackGroupsIdBodyParam = {
      name: 'string',
      slug: 'string',
      site: 8
    };
    describe('#patchDcimRackGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimRackGroupsId(dcimId, dcimPatchDcimRackGroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimRackGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRackGroupsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.rack_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRackGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackReservations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRackReservations(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRackReservations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimRackReservationsIdBodyParam = {
      rack: 2,
      units: [
        9
      ],
      user: 6,
      description: 'string'
    };
    describe('#putDcimRackReservationsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimRackReservationsId(dcimId, dcimPutDcimRackReservationsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimRackReservationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimRackReservationsIdBodyParam = {
      rack: 5,
      units: [
        5
      ],
      user: 3,
      description: 'string'
    };
    describe('#patchDcimRackReservationsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimRackReservationsId(dcimId, dcimPatchDcimRackReservationsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimRackReservationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackReservationsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRackReservationsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('object', typeof data.response.rack);
                assert.equal(true, Array.isArray(data.response.units));
                assert.equal('string', data.response.created);
                assert.equal('object', typeof data.response.user);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('string', data.response.description);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRackReservationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRackRoles(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRackRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimRackRolesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putDcimRackRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimRackRolesId(dcimId, dcimPutDcimRackRolesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimRackRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimRackRolesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchDcimRackRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimRackRolesId(dcimId, dcimPatchDcimRackRolesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimRackRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRackRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRackRolesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.description);
                assert.equal(5, data.response.rack_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRackRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRacks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRacks(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRacks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimRacksIdBodyParam = {
      name: 'string',
      site: 1
    };
    describe('#putDcimRacksId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimRacksId(dcimId, dcimPutDcimRacksIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimRacksId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimRacksIdBodyParam = {
      name: 'string',
      site: 9
    };
    describe('#patchDcimRacksId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimRacksId(dcimId, dcimPatchDcimRacksIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimRacksId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRacksId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRacksId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.facilityId);
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.group);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.role);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.assetTag);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.width);
                assert.equal(4, data.response.uHeight);
                assert.equal(false, data.response.descUnits);
                assert.equal(8, data.response.outerWidth);
                assert.equal(2, data.response.outerDepth);
                assert.equal('object', typeof data.response.outerUnit);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(10, data.response.device_count);
                assert.equal(7, data.response.powerfeed_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRacksId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRacksIdElevation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRacksIdElevation(dcimId, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRacksIdElevation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRearPortTemplates(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRearPortTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimRearPortTemplatesIdBodyParam = {
      device_type: 4,
      name: 'string',
      type: 'fc'
    };
    describe('#putDcimRearPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimRearPortTemplatesId(dcimId, dcimPutDcimRearPortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimRearPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimRearPortTemplatesIdBodyParam = {
      device_type: 8,
      name: 'string',
      type: 'sc'
    };
    describe('#patchDcimRearPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimRearPortTemplatesId(dcimId, dcimPatchDcimRearPortTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimRearPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRearPortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('object', typeof data.response.device_type);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(9, data.response.positions);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRearPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRearPorts(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRearPorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimRearPortsIdBodyParam = {
      device: 9,
      name: 'string',
      type: 'mrj21'
    };
    describe('#putDcimRearPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimRearPortsId(dcimId, dcimPutDcimRearPortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimRearPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimRearPortsIdBodyParam = {
      device: 6,
      name: 'string',
      type: 'lsh-apc'
    };
    describe('#patchDcimRearPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimRearPortsId(dcimId, dcimPatchDcimRearPortsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimRearPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRearPortsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRearPortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(6, data.response.positions);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.cable);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRearPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRegions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRegions(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRegions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimRegionsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putDcimRegionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimRegionsId(dcimId, dcimPutDcimRegionsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimRegionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimRegionsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchDcimRegionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimRegionsId(dcimId, dcimPatchDcimRegionsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimRegionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimRegionsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimRegionsId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.site_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimRegionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimSites(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimSitesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putDcimSitesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimSitesId(dcimId, dcimPutDcimSitesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimSitesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimSitesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchDcimSitesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimSitesId(dcimId, dcimPatchDcimSitesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimSitesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimSitesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimSitesId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('string', data.response.facility);
                assert.equal(7, data.response.asn);
                assert.equal('string', data.response.time_zone);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.physical_address);
                assert.equal('string', data.response.shipping_address);
                assert.equal('string', data.response.latitude);
                assert.equal('string', data.response.longitude);
                assert.equal('string', data.response.contactName);
                assert.equal('string', data.response.contactPhone);
                assert.equal('string', data.response.contactEmail);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(4, data.response.circuit_count);
                assert.equal(7, data.response.device_count);
                assert.equal(9, data.response.prefix_count);
                assert.equal(9, data.response.rack_count);
                assert.equal(6, data.response.virtualmachine_count);
                assert.equal(8, data.response.vlan_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimSitesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimSitesIdGraphs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimSitesIdGraphs(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.region);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('string', data.response.facility);
                assert.equal(10, data.response.asn);
                assert.equal('string', data.response.time_zone);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.physical_address);
                assert.equal('string', data.response.shipping_address);
                assert.equal('string', data.response.latitude);
                assert.equal('string', data.response.longitude);
                assert.equal('string', data.response.contactName);
                assert.equal('string', data.response.contactPhone);
                assert.equal('string', data.response.contactEmail);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(5, data.response.circuit_count);
                assert.equal(8, data.response.device_count);
                assert.equal(6, data.response.prefix_count);
                assert.equal(7, data.response.rack_count);
                assert.equal(10, data.response.virtualmachine_count);
                assert.equal(5, data.response.vlan_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimSitesIdGraphs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimVirtualChassis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimVirtualChassis(dcimId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimVirtualChassis', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimVirtualChassisIdBodyParam = {
      master: 6
    };
    describe('#putDcimVirtualChassisId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putDcimVirtualChassisId(dcimId, dcimPutDcimVirtualChassisIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimVirtualChassisId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimVirtualChassisIdBodyParam = {
      master: 6
    };
    describe('#patchDcimVirtualChassisId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchDcimVirtualChassisId(dcimId, dcimPatchDcimVirtualChassisIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimVirtualChassisId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimVirtualChassisId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimVirtualChassisId(dcimId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('object', typeof data.response.master);
                assert.equal('string', data.response.domain);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal(6, data.response.member_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimVirtualChassisId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let extrasId = 'fakedata';
    const extrasPostExtrasConfigContextsBodyParam = {
      name: 'string',
      data: 'string'
    };
    describe('#postExtrasConfigContexts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postExtrasConfigContexts(extrasPostExtrasConfigContextsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(5, data.response.weight);
                assert.equal('string', data.response.description);
                assert.equal(false, data.response.isActive);
                assert.equal(true, Array.isArray(data.response.regions));
                assert.equal(true, Array.isArray(data.response.sites));
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(true, Array.isArray(data.response.platforms));
                assert.equal(true, Array.isArray(data.response.cluster_groups));
                assert.equal(true, Array.isArray(data.response.clusters));
                assert.equal(true, Array.isArray(data.response.tenant_groups));
                assert.equal(true, Array.isArray(data.response.tenants));
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              extrasId = data.response.id;
              saveMockData('Extras', 'postExtrasConfigContexts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPostExtrasExportTemplatesBodyParam = {
      content_type: 'string',
      name: 'string',
      template_code: 'string'
    };
    describe('#postExtrasExportTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postExtrasExportTemplates(extrasPostExtrasExportTemplatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.contentType);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.templateLanguage);
                assert.equal('string', data.response.template_code);
                assert.equal('string', data.response.mime_type);
                assert.equal('string', data.response.file_extension);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'postExtrasExportTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPostExtrasGraphsBodyParam = {
      type: 'string',
      name: 'string',
      source: 'string'
    };
    describe('#postExtrasGraphs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postExtrasGraphs(extrasPostExtrasGraphsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.type);
                assert.equal(3, data.response.weight);
                assert.equal('string', data.response.name);
                assert.equal('jinja2', data.response.templateLanguage);
                assert.equal('string', data.response.source);
                assert.equal('string', data.response.link);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'postExtrasGraphs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPostExtrasImageAttachmentsBodyParam = {
      content_type: 'string',
      object_id: 6,
      image_height: 7,
      image_width: 2
    };
    describe('#postExtrasImageAttachments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postExtrasImageAttachments(extrasPostExtrasImageAttachmentsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.contentType);
                assert.equal(9, data.response.object_id);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.image);
                assert.equal(3, data.response.image_height);
                assert.equal(3, data.response.image_width);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'postExtrasImageAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExtrasReportsIdRun - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postExtrasReportsIdRun(extrasId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'postExtrasReportsIdRun', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPostExtrasTagsBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postExtrasTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postExtrasTags(extrasPostExtrasTagsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.description);
                assert.equal(4, data.response.tagged_items);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'postExtrasTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomFieldChoices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtrasCustomFieldChoices((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasCustomFieldChoices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasCustomFieldChoicesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtrasCustomFieldChoicesId(extrasId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasCustomFieldChoicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasConfigContexts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasConfigContexts(extrasId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasConfigContexts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPutExtrasConfigContextsIdBodyParam = {
      name: 'string',
      data: 'string'
    };
    describe('#putExtrasConfigContextsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putExtrasConfigContextsId(extrasId, extrasPutExtrasConfigContextsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'putExtrasConfigContextsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPatchExtrasConfigContextsIdBodyParam = {
      name: 'string',
      data: 'string'
    };
    describe('#patchExtrasConfigContextsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchExtrasConfigContextsId(extrasId, extrasPatchExtrasConfigContextsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'patchExtrasConfigContextsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasConfigContextsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasConfigContextsId(extrasId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.weight);
                assert.equal('string', data.response.description);
                assert.equal(true, data.response.isActive);
                assert.equal(true, Array.isArray(data.response.regions));
                assert.equal(true, Array.isArray(data.response.sites));
                assert.equal(true, Array.isArray(data.response.roles));
                assert.equal(true, Array.isArray(data.response.platforms));
                assert.equal(true, Array.isArray(data.response.cluster_groups));
                assert.equal(true, Array.isArray(data.response.clusters));
                assert.equal(true, Array.isArray(data.response.tenant_groups));
                assert.equal(true, Array.isArray(data.response.tenants));
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasConfigContextsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasExportTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasExportTemplates(extrasId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasExportTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPutExtrasExportTemplatesIdBodyParam = {
      content_type: 'string',
      name: 'string',
      template_code: 'string'
    };
    describe('#putExtrasExportTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putExtrasExportTemplatesId(extrasId, extrasPutExtrasExportTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'putExtrasExportTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPatchExtrasExportTemplatesIdBodyParam = {
      content_type: 'string',
      name: 'string',
      template_code: 'string'
    };
    describe('#patchExtrasExportTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchExtrasExportTemplatesId(extrasId, extrasPatchExtrasExportTemplatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'patchExtrasExportTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasExportTemplatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasExportTemplatesId(extrasId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.id);
                assert.equal('string', data.response.contentType);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.templateLanguage);
                assert.equal('string', data.response.template_code);
                assert.equal('string', data.response.mime_type);
                assert.equal('string', data.response.file_extension);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasExportTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasGraphs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasGraphs(extrasId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasGraphs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPutExtrasGraphsIdBodyParam = {
      type: 'string',
      name: 'string',
      source: 'string'
    };
    describe('#putExtrasGraphsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putExtrasGraphsId(extrasId, extrasPutExtrasGraphsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'putExtrasGraphsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPatchExtrasGraphsIdBodyParam = {
      type: 'string',
      name: 'string',
      source: 'string'
    };
    describe('#patchExtrasGraphsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchExtrasGraphsId(extrasId, extrasPatchExtrasGraphsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'patchExtrasGraphsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasGraphsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasGraphsId(extrasId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.type);
                assert.equal(2, data.response.weight);
                assert.equal('string', data.response.name);
                assert.equal('jinja2', data.response.templateLanguage);
                assert.equal('string', data.response.source);
                assert.equal('string', data.response.link);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasGraphsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasImageAttachments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasImageAttachments(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasImageAttachments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPutExtrasImageAttachmentsIdBodyParam = {
      content_type: 'string',
      object_id: 7,
      image_height: 5,
      image_width: 8
    };
    describe('#putExtrasImageAttachmentsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putExtrasImageAttachmentsId(extrasId, extrasPutExtrasImageAttachmentsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'putExtrasImageAttachmentsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPatchExtrasImageAttachmentsIdBodyParam = {
      content_type: 'string',
      object_id: 6,
      image_height: 10,
      image_width: 9
    };
    describe('#patchExtrasImageAttachmentsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchExtrasImageAttachmentsId(extrasId, extrasPatchExtrasImageAttachmentsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'patchExtrasImageAttachmentsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasImageAttachmentsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasImageAttachmentsId(extrasId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.contentType);
                assert.equal(4, data.response.object_id);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.image);
                assert.equal(3, data.response.image_height);
                assert.equal(10, data.response.image_width);
                assert.equal('string', data.response.created);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasImageAttachmentsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasObjectChanges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasObjectChanges(extrasId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasObjectChanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasObjectChangesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasObjectChangesId(extrasId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.time);
                assert.equal('object', typeof data.response.user);
                assert.equal('string', data.response.userName);
                assert.equal('string', data.response.requestId);
                assert.equal('object', typeof data.response.action);
                assert.equal('string', data.response.changedObjectType);
                assert.equal(3, data.response.changedObjectId);
                assert.equal('object', typeof data.response.changed_object);
                assert.equal('string', data.response.object_data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasObjectChangesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasReports - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtrasReports((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasReports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasReportsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtrasReportsId(extrasId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasReportsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasScripts - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtrasScripts((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasScripts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasScriptsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getExtrasScriptsId(extrasId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasScriptsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasTags - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasTags(extrasId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasTags', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPutExtrasTagsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putExtrasTagsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putExtrasTagsId(extrasId, extrasPutExtrasTagsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'putExtrasTagsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const extrasPatchExtrasTagsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchExtrasTagsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchExtrasTagsId(extrasId, extrasPatchExtrasTagsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'patchExtrasTagsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtrasTagsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExtrasTagsId(extrasId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.color);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.tagged_items);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'getExtrasTagsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let ipamId = 'fakedata';
    const ipamPostIpamAggregatesBodyParam = {
      prefix: 'string',
      rir: 7
    };
    describe('#postIpamAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamAggregates(ipamPostIpamAggregatesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('object', typeof data.response.family);
                assert.equal('string', data.response.prefix);
                assert.equal('object', typeof data.response.rir);
                assert.equal('string', data.response.dateAdded);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              ipamId = data.response.id;
              saveMockData('Ipam', 'postIpamAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamIpAddressesBodyParam = {
      address: 'string',
      nat_outside: 6
    };
    describe('#postIpamIpAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamIpAddresses(ipamPostIpamIpAddressesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('object', typeof data.response.family);
                assert.equal('string', data.response.address);
                assert.equal('object', typeof data.response.vrf);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.role);
                assert.equal('object', typeof data.response.interfaceParam);
                assert.equal('object', typeof data.response.nat_inside);
                assert.equal('object', typeof data.response.nat_outside);
                assert.equal('string', data.response.dnsName);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamIpAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamPrefixesBodyParam = {
      prefix: 'string'
    };
    describe('#postIpamPrefixes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamPrefixes(ipamPostIpamPrefixesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('object', typeof data.response.family);
                assert.equal('string', data.response.prefix);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.vrf);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.vlan);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.role);
                assert.equal(false, data.response.isPool);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamPrefixes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamPrefixesIdAvailableIpsBodyParam = {
      prefix: 'string'
    };
    describe('#postIpamPrefixesIdAvailableIps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamPrefixesIdAvailableIps(ipamId, ipamPostIpamPrefixesIdAvailableIpsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamPrefixesIdAvailableIps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamPrefixesIdAvailablePrefixesBodyParam = {
      prefix: 'string'
    };
    describe('#postIpamPrefixesIdAvailablePrefixes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamPrefixesIdAvailablePrefixes(ipamId, ipamPostIpamPrefixesIdAvailablePrefixesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamPrefixesIdAvailablePrefixes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamRirsBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postIpamRirs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamRirs(ipamPostIpamRirsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal(true, data.response.isPrivate);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.aggregate_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamRirs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamRolesBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postIpamRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamRoles(ipamPostIpamRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal(6, data.response.weight);
                assert.equal('string', data.response.description);
                assert.equal(4, data.response.prefix_count);
                assert.equal(7, data.response.vlan_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamServicesBodyParam = {
      name: 'string',
      port: 9,
      protocol: 'udp'
    };
    describe('#postIpamServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamServices(ipamPostIpamServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('object', typeof data.response.virtualMachine);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.port);
                assert.equal('object', typeof data.response.protocol);
                assert.equal(true, Array.isArray(data.response.ipaddresses));
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamVlanGroupsBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postIpamVlanGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamVlanGroups(ipamPostIpamVlanGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.site);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.vlan_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamVlanGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamVlansBodyParam = {
      vid: 8,
      name: 'string'
    };
    describe('#postIpamVlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamVlans(ipamPostIpamVlansBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.group);
                assert.equal(6, data.response.vid);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.role);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(1, data.response.prefix_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamVlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPostIpamVrfsBodyParam = {
      name: 'string'
    };
    describe('#postIpamVrfs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postIpamVrfs(ipamPostIpamVrfsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.rd);
                assert.equal('object', typeof data.response.tenant);
                assert.equal(false, data.response.enforceUnique);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(4, data.response.ipaddress_count);
                assert.equal(8, data.response.prefix_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'postIpamVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAggregates(ipamId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAggregates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPutIpamAggregatesIdBodyParam = {
      prefix: 'string',
      rir: 9
    };
    describe('#putIpamAggregatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamAggregatesId(ipamId, ipamPutIpamAggregatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'putIpamAggregatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPatchIpamAggregatesIdBodyParam = {
      prefix: 'string',
      rir: 5
    };
    describe('#patchIpamAggregatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIpamAggregatesId(ipamId, ipamPatchIpamAggregatesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'patchIpamAggregatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamAggregatesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamAggregatesId(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.family);
                assert.equal('string', data.response.prefix);
                assert.equal('object', typeof data.response.rir);
                assert.equal('string', data.response.dateAdded);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamAggregatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamIpAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamIpAddresses(ipamId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamIpAddresses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPutIpamIpAddressesIdBodyParam = {
      address: 'string',
      nat_outside: 10
    };
    describe('#putIpamIpAddressesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamIpAddressesId(ipamId, ipamPutIpamIpAddressesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'putIpamIpAddressesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPatchIpamIpAddressesIdBodyParam = {
      address: 'string',
      nat_outside: 10
    };
    describe('#patchIpamIpAddressesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIpamIpAddressesId(ipamId, ipamPatchIpamIpAddressesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'patchIpamIpAddressesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamIpAddressesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamIpAddressesId(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('object', typeof data.response.family);
                assert.equal('string', data.response.address);
                assert.equal('object', typeof data.response.vrf);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.role);
                assert.equal('object', typeof data.response.interfaceParam);
                assert.equal('object', typeof data.response.nat_inside);
                assert.equal('object', typeof data.response.nat_outside);
                assert.equal('string', data.response.dnsName);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamIpAddressesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPrefixes(ipamId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPrefixes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPutIpamPrefixesIdBodyParam = {
      prefix: 'string'
    };
    describe('#putIpamPrefixesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamPrefixesId(ipamId, ipamPutIpamPrefixesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'putIpamPrefixesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPatchIpamPrefixesIdBodyParam = {
      prefix: 'string'
    };
    describe('#patchIpamPrefixesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIpamPrefixesId(ipamId, ipamPatchIpamPrefixesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'patchIpamPrefixesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPrefixesId(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('object', typeof data.response.family);
                assert.equal('string', data.response.prefix);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.vrf);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.vlan);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.role);
                assert.equal(true, data.response.isPool);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPrefixesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixesIdAvailableIps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPrefixesIdAvailableIps(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPrefixesIdAvailableIps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamPrefixesIdAvailablePrefixes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamPrefixesIdAvailablePrefixes(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamPrefixesIdAvailablePrefixes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRirs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamRirs(ipamId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamRirs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPutIpamRirsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putIpamRirsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamRirsId(ipamId, ipamPutIpamRirsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'putIpamRirsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPatchIpamRirsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchIpamRirsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIpamRirsId(ipamId, ipamPatchIpamRirsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'patchIpamRirsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRirsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamRirsId(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal(true, data.response.isPrivate);
                assert.equal('string', data.response.description);
                assert.equal(6, data.response.aggregate_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamRirsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamRoles(ipamId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPutIpamRolesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putIpamRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamRolesId(ipamId, ipamPutIpamRolesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'putIpamRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPatchIpamRolesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchIpamRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIpamRolesId(ipamId, ipamPatchIpamRolesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'patchIpamRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamRolesId(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal(3, data.response.weight);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.prefix_count);
                assert.equal(1, data.response.vlan_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamServices(ipamId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPutIpamServicesIdBodyParam = {
      name: 'string',
      port: 10,
      protocol: 'udp'
    };
    describe('#putIpamServicesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamServicesId(ipamId, ipamPutIpamServicesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'putIpamServicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPatchIpamServicesIdBodyParam = {
      name: 'string',
      port: 7,
      protocol: 'udp'
    };
    describe('#patchIpamServicesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIpamServicesId(ipamId, ipamPatchIpamServicesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'patchIpamServicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamServicesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamServicesId(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('object', typeof data.response.virtualMachine);
                assert.equal('string', data.response.name);
                assert.equal(4, data.response.port);
                assert.equal('object', typeof data.response.protocol);
                assert.equal(true, Array.isArray(data.response.ipaddresses));
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamServicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlanGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamVlanGroups(ipamId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamVlanGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPutIpamVlanGroupsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putIpamVlanGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamVlanGroupsId(ipamId, ipamPutIpamVlanGroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'putIpamVlanGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPatchIpamVlanGroupsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchIpamVlanGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIpamVlanGroupsId(ipamId, ipamPatchIpamVlanGroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'patchIpamVlanGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlanGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamVlanGroupsId(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.site);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.vlan_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamVlanGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamVlans(ipamId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamVlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPutIpamVlansIdBodyParam = {
      vid: 9,
      name: 'string'
    };
    describe('#putIpamVlansId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamVlansId(ipamId, ipamPutIpamVlansIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'putIpamVlansId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPatchIpamVlansIdBodyParam = {
      vid: 9,
      name: 'string'
    };
    describe('#patchIpamVlansId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIpamVlansId(ipamId, ipamPatchIpamVlansIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'patchIpamVlansId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVlansId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamVlansId(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.group);
                assert.equal(5, data.response.vid);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.role);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(9, data.response.prefix_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamVlansId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVrfs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamVrfs(ipamId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPutIpamVrfsIdBodyParam = {
      name: 'string'
    };
    describe('#putIpamVrfsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putIpamVrfsId(ipamId, ipamPutIpamVrfsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'putIpamVrfsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ipamPatchIpamVrfsIdBodyParam = {
      name: 'string'
    };
    describe('#patchIpamVrfsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchIpamVrfsId(ipamId, ipamPatchIpamVrfsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'patchIpamVrfsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpamVrfsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIpamVrfsId(ipamId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.rd);
                assert.equal('object', typeof data.response.tenant);
                assert.equal(false, data.response.enforceUnique);
                assert.equal('string', data.response.description);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.display_name);
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(3, data.response.ipaddress_count);
                assert.equal(2, data.response.prefix_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'getIpamVrfsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretsGetSessionKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSecretsGetSessionKey((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'postSecretsGetSessionKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let secretsId = 'fakedata';
    const secretsPostSecretsSecretRolesBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postSecretsSecretRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSecretsSecretRoles(secretsPostSecretsSecretRolesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(5, data.response.secret_count);
              } else {
                runCommonAsserts(data, error);
              }
              secretsId = data.response.id;
              saveMockData('Secrets', 'postSecretsSecretRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsPostSecretsSecretsBodyParam = {
      device: 10,
      role: 2,
      plaintext: 'string'
    };
    describe('#postSecretsSecrets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSecretsSecrets(secretsPostSecretsSecretsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('object', typeof data.response.role);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.plaintext);
                assert.equal('string', data.response.hash);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'postSecretsSecrets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsGenerateRsaKeyPair - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getSecretsGenerateRsaKeyPair((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'getSecretsGenerateRsaKeyPair', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsSecretRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecretsSecretRoles(secretsId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'getSecretsSecretRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsPutSecretsSecretRolesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putSecretsSecretRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSecretsSecretRolesId(secretsId, secretsPutSecretsSecretRolesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'putSecretsSecretRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsPatchSecretsSecretRolesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchSecretsSecretRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchSecretsSecretRolesId(secretsId, secretsPatchSecretsSecretRolesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'patchSecretsSecretRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsSecretRolesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecretsSecretRolesId(secretsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(4, data.response.secret_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'getSecretsSecretRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsSecrets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecretsSecrets(secretsId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'getSecretsSecrets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsPutSecretsSecretsIdBodyParam = {
      device: 7,
      role: 4,
      plaintext: 'string'
    };
    describe('#putSecretsSecretsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putSecretsSecretsId(secretsId, secretsPutSecretsSecretsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'putSecretsSecretsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const secretsPatchSecretsSecretsIdBodyParam = {
      device: 7,
      role: 5,
      plaintext: 'string'
    };
    describe('#patchSecretsSecretsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchSecretsSecretsId(secretsId, secretsPatchSecretsSecretsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'patchSecretsSecretsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsSecretsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecretsSecretsId(secretsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.id);
                assert.equal('object', typeof data.response.device);
                assert.equal('object', typeof data.response.role);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.plaintext);
                assert.equal('string', data.response.hash);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'getSecretsSecretsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let tenancyId = 'fakedata';
    const tenancyPostTenancyTenantGroupsBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postTenancyTenantGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTenancyTenantGroups(tenancyPostTenancyTenantGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.description);
                assert.equal(7, data.response.tenant_count);
              } else {
                runCommonAsserts(data, error);
              }
              tenancyId = data.response.id;
              saveMockData('Tenancy', 'postTenancyTenantGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenancyPostTenancyTenantsBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postTenancyTenants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTenancyTenants(tenancyPostTenancyTenantsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.group);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(2, data.response.circuit_count);
                assert.equal(3, data.response.device_count);
                assert.equal(7, data.response.ipaddress_count);
                assert.equal(3, data.response.prefix_count);
                assert.equal(7, data.response.rack_count);
                assert.equal(9, data.response.site_count);
                assert.equal(3, data.response.virtualmachine_count);
                assert.equal(10, data.response.vlan_count);
                assert.equal(2, data.response.vrf_count);
                assert.equal(2, data.response.cluster_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'postTenancyTenants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenantGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenancyTenantGroups(tenancyId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'getTenancyTenantGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenancyPutTenancyTenantGroupsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putTenancyTenantGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putTenancyTenantGroupsId(tenancyId, tenancyPutTenancyTenantGroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'putTenancyTenantGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenancyPatchTenancyTenantGroupsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchTenancyTenantGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchTenancyTenantGroupsId(tenancyId, tenancyPatchTenancyTenantGroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'patchTenancyTenantGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenantGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenancyTenantGroupsId(tenancyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.parent);
                assert.equal('string', data.response.description);
                assert.equal(4, data.response.tenant_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'getTenancyTenantGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenants - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenancyTenants(tenancyId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'getTenancyTenants', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenancyPutTenancyTenantsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putTenancyTenantsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putTenancyTenantsId(tenancyId, tenancyPutTenancyTenantsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'putTenancyTenantsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tenancyPatchTenancyTenantsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchTenancyTenantsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchTenancyTenantsId(tenancyId, tenancyPatchTenancyTenantsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'patchTenancyTenantsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTenancyTenantsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTenancyTenantsId(tenancyId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('object', typeof data.response.group);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(7, data.response.circuit_count);
                assert.equal(5, data.response.device_count);
                assert.equal(1, data.response.ipaddress_count);
                assert.equal(7, data.response.prefix_count);
                assert.equal(5, data.response.rack_count);
                assert.equal(1, data.response.site_count);
                assert.equal(5, data.response.virtualmachine_count);
                assert.equal(8, data.response.vlan_count);
                assert.equal(10, data.response.vrf_count);
                assert.equal(1, data.response.cluster_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'getTenancyTenantsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let virtualizationId = 'fakedata';
    const virtualizationPostVirtualizationClusterGroupsBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postVirtualizationClusterGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVirtualizationClusterGroups(virtualizationPostVirtualizationClusterGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(8, data.response.cluster_count);
              } else {
                runCommonAsserts(data, error);
              }
              virtualizationId = data.response.id;
              saveMockData('Virtualization', 'postVirtualizationClusterGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPostVirtualizationClusterTypesBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#postVirtualizationClusterTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVirtualizationClusterTypes(virtualizationPostVirtualizationClusterTypesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.cluster_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'postVirtualizationClusterTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPostVirtualizationClustersBodyParam = {
      name: 'string',
      type: 3
    };
    describe('#postVirtualizationClusters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVirtualizationClusters(virtualizationPostVirtualizationClustersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.group);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.site);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(8, data.response.device_count);
                assert.equal(6, data.response.virtualmachine_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'postVirtualizationClusters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPostVirtualizationInterfacesBodyParam = {
      name: 'string',
      type: 'infiniband-hdr'
    };
    describe('#postVirtualizationInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVirtualizationInterfaces(virtualizationPostVirtualizationInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('object', typeof data.response.virtualMachine);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(true, data.response.enabled);
                assert.equal(9, data.response.mtu);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.mode);
                assert.equal('object', typeof data.response.untagged_vlan);
                assert.equal(true, Array.isArray(data.response.tagged_vlans));
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'postVirtualizationInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPostVirtualizationVirtualMachinesBodyParam = {
      name: 'string',
      cluster: 8
    };
    describe('#postVirtualizationVirtualMachines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVirtualizationVirtualMachines(virtualizationPostVirtualizationVirtualMachinesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.cluster);
                assert.equal('object', typeof data.response.role);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.platform);
                assert.equal('object', typeof data.response.primary_ip);
                assert.equal('object', typeof data.response.primary_ip4);
                assert.equal('object', typeof data.response.primary_ip6);
                assert.equal(4, data.response.vcpus);
                assert.equal(1, data.response.memory);
                assert.equal(2, data.response.disk);
                assert.equal('string', data.response.comments);
                assert.equal('string', data.response.localContextData);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('object', typeof data.response.config_context);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'postVirtualizationVirtualMachines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationClusterGroups(virtualizationId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationClusterGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPutVirtualizationClusterGroupsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putVirtualizationClusterGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putVirtualizationClusterGroupsId(virtualizationId, virtualizationPutVirtualizationClusterGroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'putVirtualizationClusterGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPatchVirtualizationClusterGroupsIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchVirtualizationClusterGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchVirtualizationClusterGroupsId(virtualizationId, virtualizationPatchVirtualizationClusterGroupsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'patchVirtualizationClusterGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterGroupsId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationClusterGroupsId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.cluster_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationClusterGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationClusterTypes(virtualizationId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationClusterTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPutVirtualizationClusterTypesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#putVirtualizationClusterTypesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putVirtualizationClusterTypesId(virtualizationId, virtualizationPutVirtualizationClusterTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'putVirtualizationClusterTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPatchVirtualizationClusterTypesIdBodyParam = {
      name: 'string',
      slug: 'string'
    };
    describe('#patchVirtualizationClusterTypesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchVirtualizationClusterTypesId(virtualizationId, virtualizationPatchVirtualizationClusterTypesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'patchVirtualizationClusterTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusterTypesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationClusterTypesId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.slug);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.cluster_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationClusterTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClusters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationClusters(virtualizationId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationClusters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPutVirtualizationClustersIdBodyParam = {
      name: 'string',
      type: 3
    };
    describe('#putVirtualizationClustersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putVirtualizationClustersId(virtualizationId, virtualizationPutVirtualizationClustersIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'putVirtualizationClustersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPatchVirtualizationClustersIdBodyParam = {
      name: 'string',
      type: 2
    };
    describe('#patchVirtualizationClustersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchVirtualizationClustersId(virtualizationId, virtualizationPatchVirtualizationClustersIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'patchVirtualizationClustersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationClustersId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationClustersId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal('object', typeof data.response.group);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.site);
                assert.equal('string', data.response.comments);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
                assert.equal(6, data.response.device_count);
                assert.equal(5, data.response.virtualmachine_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationClustersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationInterfaces(virtualizationId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPutVirtualizationInterfacesIdBodyParam = {
      name: 'string',
      type: 'gsm'
    };
    describe('#putVirtualizationInterfacesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putVirtualizationInterfacesId(virtualizationId, virtualizationPutVirtualizationInterfacesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'putVirtualizationInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPatchVirtualizationInterfacesIdBodyParam = {
      name: 'string',
      type: 'cisco-flexstack'
    };
    describe('#patchVirtualizationInterfacesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchVirtualizationInterfacesId(virtualizationId, virtualizationPatchVirtualizationInterfacesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'patchVirtualizationInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationInterfacesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationInterfacesId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.id);
                assert.equal('object', typeof data.response.virtualMachine);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.type);
                assert.equal(true, data.response.enabled);
                assert.equal(2, data.response.mtu);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.mode);
                assert.equal('object', typeof data.response.untagged_vlan);
                assert.equal(true, Array.isArray(data.response.tagged_vlans));
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationVirtualMachines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationVirtualMachines(virtualizationId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationVirtualMachines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPutVirtualizationVirtualMachinesIdBodyParam = {
      name: 'string',
      cluster: 9
    };
    describe('#putVirtualizationVirtualMachinesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putVirtualizationVirtualMachinesId(virtualizationId, virtualizationPutVirtualizationVirtualMachinesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'putVirtualizationVirtualMachinesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualizationPatchVirtualizationVirtualMachinesIdBodyParam = {
      name: 'string',
      cluster: 1
    };
    describe('#patchVirtualizationVirtualMachinesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchVirtualizationVirtualMachinesId(virtualizationId, virtualizationPatchVirtualizationVirtualMachinesIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'patchVirtualizationVirtualMachinesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVirtualizationVirtualMachinesId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVirtualizationVirtualMachinesId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.status);
                assert.equal('object', typeof data.response.site);
                assert.equal('object', typeof data.response.cluster);
                assert.equal('object', typeof data.response.role);
                assert.equal('object', typeof data.response.tenant);
                assert.equal('object', typeof data.response.platform);
                assert.equal('object', typeof data.response.primary_ip);
                assert.equal('object', typeof data.response.primary_ip4);
                assert.equal('object', typeof data.response.primary_ip6);
                assert.equal(2, data.response.vcpus);
                assert.equal(3, data.response.memory);
                assert.equal(5, data.response.disk);
                assert.equal('string', data.response.comments);
                assert.equal('string', data.response.localContextData);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('object', typeof data.response.custom_fields);
                assert.equal('object', typeof data.response.config_context);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.lastUpdated);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'getVirtualizationVirtualMachinesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTerminationsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCircuitsCircuitTerminationsId(circuitsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'deleteCircuitsCircuitTerminationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitTypesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCircuitsCircuitTypesId(circuitsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'deleteCircuitsCircuitTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsCircuitsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCircuitsCircuitsId(circuitsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'deleteCircuitsCircuitsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCircuitsProvidersId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteCircuitsProvidersId(circuitsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Circuits', 'deleteCircuitsProvidersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimCablesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimCablesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimCablesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePortTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimConsolePortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimConsolePortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsolePortsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimConsolePortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimConsolePortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPortTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimConsoleServerPortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimConsoleServerPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimConsoleServerPortsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimConsoleServerPortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimConsoleServerPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBayTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimDeviceBayTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimDeviceBayTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceBaysId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimDeviceBaysId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimDeviceBaysId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceRolesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimDeviceRolesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimDeviceRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDeviceTypesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimDeviceTypesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimDeviceTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimDevicesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimDevicesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimDevicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPortTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimFrontPortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimFrontPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimFrontPortsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimFrontPortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimFrontPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfaceTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimInterfaceTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimInterfaceTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInterfacesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimInterfacesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimInventoryItemsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimInventoryItemsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimInventoryItemsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimManufacturersId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimManufacturersId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimManufacturersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPlatformsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimPlatformsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimPlatformsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerFeedsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimPowerFeedsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimPowerFeedsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutletTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimPowerOutletTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimPowerOutletTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerOutletsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimPowerOutletsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimPowerOutletsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPanelsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimPowerPanelsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimPowerPanelsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPortTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimPowerPortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimPowerPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimPowerPortsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimPowerPortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimPowerPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackGroupsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimRackGroupsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimRackGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackReservationsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimRackReservationsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimRackReservationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRackRolesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimRackRolesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimRackRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRacksId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimRacksId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimRacksId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPortTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimRearPortTemplatesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimRearPortTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRearPortsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimRearPortsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimRearPortsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimRegionsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimRegionsId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimRegionsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimSitesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimSitesId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimSitesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimVirtualChassisId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimVirtualChassisId(dcimId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimVirtualChassisId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasConfigContextsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExtrasConfigContextsId(extrasId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'deleteExtrasConfigContextsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasExportTemplatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExtrasExportTemplatesId(extrasId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'deleteExtrasExportTemplatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasGraphsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExtrasGraphsId(extrasId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'deleteExtrasGraphsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasImageAttachmentsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExtrasImageAttachmentsId(extrasId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'deleteExtrasImageAttachmentsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExtrasTagsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExtrasTagsId(extrasId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Extras', 'deleteExtrasTagsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamAggregatesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamAggregatesId(ipamId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamAggregatesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamIpAddressesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamIpAddressesId(ipamId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamIpAddressesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamPrefixesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamPrefixesId(ipamId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamPrefixesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRirsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamRirsId(ipamId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamRirsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamRolesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamRolesId(ipamId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamServicesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamServicesId(ipamId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamServicesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlanGroupsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamVlanGroupsId(ipamId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamVlanGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVlansId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamVlansId(ipamId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamVlansId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteIpamVrfsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteIpamVrfsId(ipamId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ipam', 'deleteIpamVrfsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretsSecretRolesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecretsSecretRolesId(secretsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'deleteSecretsSecretRolesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretsSecretsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecretsSecretsId(secretsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Secrets', 'deleteSecretsSecretsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenantGroupsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTenancyTenantGroupsId(tenancyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'deleteTenancyTenantGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTenancyTenantsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTenancyTenantsId(tenancyId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tenancy', 'deleteTenancyTenantsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterGroupsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVirtualizationClusterGroupsId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'deleteVirtualizationClusterGroupsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClusterTypesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVirtualizationClusterTypesId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'deleteVirtualizationClusterTypesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationClustersId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVirtualizationClustersId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'deleteVirtualizationClustersId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationInterfacesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVirtualizationInterfacesId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'deleteVirtualizationInterfacesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVirtualizationVirtualMachinesId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVirtualizationVirtualMachinesId(virtualizationId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualization', 'deleteVirtualizationVirtualMachinesId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGraphql - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getGraphql({ query: 'fakeq' }, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Status', 'getGraphql', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimLocations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDcimLocations(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal('string', data.response.next);
                assert.equal('string', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimLocations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPostDcimLocationsBodyParam = {
      name: 'string',
      slug: 'string',
      site: 1
    };
    describe('#postDcimLocations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postDcimLocations(dcimPostDcimLocationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'postDcimLocations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimLocationsBodyParam = {
      name: 'string',
      slug: 'string',
      site: 6
    };
    describe('#putDcimLocations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putDcimLocations(dcimPutDcimLocationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimLocations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimLocationsBodyParam = {
      name: 'string',
      slug: 'string',
      site: 7
    };
    describe('#patchDcimLocations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDcimLocations(dcimPatchDcimLocationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimLocations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimLocations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimLocations((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimLocations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDcimLocationsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getDcimLocationsId(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'getDcimLocationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPutDcimLocationsIdBodyParam = {
      name: 'string',
      slug: 'string',
      site: 2
    };
    describe('#putDcimLocationsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putDcimLocationsId(555, dcimPutDcimLocationsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'putDcimLocationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dcimPatchDcimLocationsIdBodyParam = {
      name: 'string',
      slug: 'string',
      site: 9
    };
    describe('#patchDcimLocationsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.patchDcimLocationsId(555, dcimPatchDcimLocationsIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'patchDcimLocationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDcimLocationsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDcimLocationsId(555, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-netbox-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dcim', 'deleteDcimLocationsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
