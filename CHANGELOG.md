
## 0.13.4 [10-15-2024]

* Changes made at 2024.10.14_21:37PM

See merge request itentialopensource/adapters/adapter-netbox!32

---

## 0.13.3 [09-12-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-netbox!30

---

## 0.13.2 [08-15-2024]

* Changes made at 2024.08.14_19:57PM

See merge request itentialopensource/adapters/adapter-netbox!29

---

## 0.13.1 [08-07-2024]

* Changes made at 2024.08.06_22:05PM

See merge request itentialopensource/adapters/adapter-netbox!28

---

## 0.13.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-netbox!27

---

## 0.12.7 [03-26-2024]

* Changes made at 2024.03.26_14:50PM

See merge request itentialopensource/adapters/inventory/adapter-netbox!26

---

## 0.12.6 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/inventory/adapter-netbox!25

---

## 0.12.5 [03-13-2024]

* Changes made at 2024.03.13_14:05PM

See merge request itentialopensource/adapters/inventory/adapter-netbox!24

---

## 0.12.4 [03-11-2024]

* Changes made at 2024.03.11_14:15PM

See merge request itentialopensource/adapters/inventory/adapter-netbox!23

---

## 0.12.3 [02-26-2024]

* Changes made at 2024.02.26_13:44PM

See merge request itentialopensource/adapters/inventory/adapter-netbox!22

---

## 0.12.2 [12-25-2023]

* update axios and metadata

See merge request itentialopensource/adapters/inventory/adapter-netbox!21

---

## 0.12.1 [12-14-2023]

* Remediation Merge Request

See merge request itentialopensource/adapters/inventory/adapter-netbox!19

---

## 0.12.0 [11-07-2023]

* more migration changes

See merge request itentialopensource/adapters/inventory/adapter-netbox!18

---

## 0.11.0 [09-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-netbox!17

---

## 0.10.0 [09-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-netbox!17

---

## 0.9.0 [08-05-2022]

* Add dcim location calls

See merge request itentialopensource/adapters/inventory/adapter-netbox!16

---

## 0.8.0 [05-23-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-netbox!15

---

## 0.7.1 [03-07-2022]

- Fix graphql with sendGetBody
- Migration
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/inventory/adapter-netbox!14

---

## 0.7.0 [02-14-2022]

- Migration and added a call for graphql

See merge request itentialopensource/adapters/inventory/adapter-netbox!13

---

## 0.6.4 [07-13-2021]

- The current healthcheck /api/docs/ returns HTML payload that the adapter does not appear able to parse as is. The /api/status returns a minimal JSON response and appears suitable for a healthcheck request to use in the adapter-netbox code.

See merge request itentialopensource/adapters/inventory/adapter-netbox!12

---

## 0.6.3 [03-17-2021]

- Fix descriptions and summaries

See merge request itentialopensource/adapters/inventory/adapter-netbox!11

---

## 0.6.2 [03-10-2021]

- Migration to bring up to the latest foundation
	- Change to .eslintignore (adapter_modification directory)
	- Change to README.md (new properties, new scripts, new processes)
	- Changes to adapterBase.js (new methods)
	- Changes to package.json (new scripts, dependencies)
	- Changes to propertiesSchema.json (new properties and changes to existing)
	- Changes to the Unit test
	- Adding several test files, utils files and .generic entity
	- Fix order of scripts and dependencies in package.json
	- Fix order of properties in propertiesSchema.json
	- Update sampleProperties, unit and integration tests to have all new properties.
	- Add all new calls to adapter.js and pronghorn.json
	- Add suspend piece to older methods

See merge request itentialopensource/adapters/inventory/adapter-netbox!10

---

## 0.6.1 [01-28-2021]

- Changed version of netbox integrating with to 2.8.5 also provide schemas and allow for custom fields in get calls to be available as filters

See merge request itentialopensource/adapters/inventory/adapter-netbox!9

---

## 0.6.0 [07-27-2020]

- Changed version of netbox integrating with to 2.8.5 also provide schemas and allow for custom fields in get calls to be available as filters

See merge request itentialopensource/adapters/inventory/adapter-netbox!8

---

## 0.5.3 [07-08-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/inventory/adapter-netbox!7

---

## 0.5.2 [01-13-2020]

- bring up to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-netbox!6

---

## 0.5.1 [11-18-2019]

- Changes to the healthcheck so it is a tested path

See merge request itentialopensource/adapters/inventory/adapter-netbox!5

---

## 0.5.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/inventory/adapter-netbox!4

---

## 0.4.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-netbox!3

---
## 0.3.0 [07-30-2019] & 0.2.0 [07-17-2019]

- Migrate to the latest adapter foundation, categorize and prepare for app artifact

See merge request itentialopensource/adapters/inventory/adapter-netbox!2

---

## 0.1.2 [04-23-2019]

- Added keys, update and set versions that this adapter was built with

See merge request itentialopensource/adapters/adapter-netbox!1

---

## 0.1.1 [04-16-2019]

- Initial Commit

See commit ca1af4e

---
