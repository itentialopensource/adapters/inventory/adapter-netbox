# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Netbox System. The API that was used to build the adapter for Netbox is usually available in the report directory of this adapter. The adapter utilizes the Netbox API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The NetBox adapter from Itential is used to integrate the Itential Automation Platform (IAP) with NetBox. With this adapter you have the ability to perform operations such as:

- Add and Remove Inventory Items.
- Create Subnet
- Create IP Record
- Get Next Available IP
- Get Circuit
- Get L2VPN
- Get Device

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
