## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for NetBox. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for NetBox.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the NetBox. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitTerminations(termSide, portSpeed, upstreamSpeed, xconnectId, q, circuitId, siteId, site, termSideN, portSpeedN, portSpeedLte, portSpeedLt, portSpeedGte, portSpeedGt, upstreamSpeedN, upstreamSpeedLte, upstreamSpeedLt, upstreamSpeedGte, upstreamSpeedGt, xconnectIdN, xconnectIdIc, xconnectIdNic, xconnectIdIew, xconnectIdNiew, xconnectIdIsw, xconnectIdNisw, xconnectIdIe, xconnectIdNie, circuitIdN, siteIdN, siteN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Circuit Terminations</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCircuitsCircuitTerminations(data, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_create</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitTerminationsId(id, callback)</td>
    <td style="padding:15px">get circuit terminations by id</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsCircuitTerminationsId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsCircuitTerminationsId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsCircuitTerminationsId(id, callback)</td>
    <td style="padding:15px">circuits_circuit-terminations_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-terminations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitTypes(id, name, slug, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get circuit types</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCircuitsCircuitTypes(data, callback)</td>
    <td style="padding:15px">circuits_circuit-types_create</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitTypesId(id, callback)</td>
    <td style="padding:15px">get circuit types by id</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsCircuitTypesId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuit-types_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsCircuitTypesId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuit-types_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsCircuitTypesId(id, callback)</td>
    <td style="padding:15px">circuits_circuit-types_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuit-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuits(id, cid, installDate, commitRate, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, providerId, provider, typeId, type, status, siteId, site, regionId, region, tag, idN, idLte, idLt, idGte, idGt, cidN, cidIc, cidNic, cidIew, cidNiew, cidIsw, cidNisw, cidIe, cidNie, installDateN, installDateLte, installDateLt, installDateGte, installDateGt, commitRateN, commitRateLte, commitRateLt, commitRateGte, commitRateGt, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, providerIdN, providerN, typeIdN, typeN, statusN, siteIdN, siteN, regionIdN, regionN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get circuits</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCircuitsCircuits(data, callback)</td>
    <td style="padding:15px">circuits_circuits_create</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsCircuitsId(id, callback)</td>
    <td style="padding:15px">get circuits by id</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsCircuitsId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuits_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsCircuitsId(id, data, callback)</td>
    <td style="padding:15px">circuits_circuits_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsCircuitsId(id, callback)</td>
    <td style="padding:15px">circuits_circuits_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/circuits/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsProviders(id, name, slug, asn, account, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, asnN, asnLte, asnLt, asnGte, asnGt, accountN, accountIc, accountNic, accountIew, accountNiew, accountIsw, accountNisw, accountIe, accountNie, regionIdN, regionN, siteIdN, siteN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get circuit providers</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCircuitsProviders(data, callback)</td>
    <td style="padding:15px">circuits_providers_create</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsProvidersId(id, callback)</td>
    <td style="padding:15px">get circuit providers by id</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCircuitsProvidersId(id, data, callback)</td>
    <td style="padding:15px">circuits_providers_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchCircuitsProvidersId(id, data, callback)</td>
    <td style="padding:15px">circuits_providers_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCircuitsProvidersId(id, callback)</td>
    <td style="padding:15px">circuits_providers_delete</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCircuitsProvidersIdGraphs(id, callback)</td>
    <td style="padding:15px">A convenience method for rendering graphs for a particular provider.</td>
    <td style="padding:15px">{base_path}/{version}/circuits/providers/{pathv1}/graphs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimCables(id, label, length, lengthUnit, q, type, status, color, deviceId, device, rackId, rack, siteId, site, tenantId, tenant, idN, idLte, idLt, idGte, idGt, labelN, labelIc, labelNic, labelIew, labelNiew, labelIsw, labelNisw, labelIe, labelNie, lengthN, lengthLte, lengthLt, lengthGte, lengthGt, lengthUnitN, typeN, statusN, colorN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM cables</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimCables(data, callback)</td>
    <td style="padding:15px">dcim_cables_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimCablesId(id, callback)</td>
    <td style="padding:15px">get DCIM cables by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimCablesId(id, data, callback)</td>
    <td style="padding:15px">dcim_cables_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimCablesId(id, data, callback)</td>
    <td style="padding:15px">dcim_cables_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimCablesId(id, callback)</td>
    <td style="padding:15px">dcim_cables_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/cables/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConnectedDevice(peerDevice, peerInterface, customFields, callback)</td>
    <td style="padding:15px">This endpoint allows a user to determine what device (if any) is connected to a given peer device and peer
interface. This is useful in a situation where a device boots with no configuration, but can detect its neighbors
via a protocol such as LLDP. Two query parameters must be included in the request:

* `peer_device`: The name of the peer device
* `peer_interface`: The name of the peer interface</td>
    <td style="padding:15px">{base_path}/{version}/dcim/connected-device/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleConnections(name, connectionStatus, site, deviceId, device, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, connectionStatusN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">dcim_console-connections_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-connections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePortTemplates(id, name, type, q, devicetypeId, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, devicetypeIdN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Console Port Templates</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimConsolePortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePortTemplatesId(id, callback)</td>
    <td style="padding:15px">get DCIM Console Port Templates by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsolePortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsolePortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsolePortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_console-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePorts(id, name, description, connectionStatus, q, regionId, region, siteId, site, deviceId, device, tag, type, cabled, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, connectionStatusN, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, typeN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Console Ports</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimConsolePorts(data, callback)</td>
    <td style="padding:15px">dcim_console-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePortsId(id, callback)</td>
    <td style="padding:15px">get DCIM Console Ports by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsolePortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsolePortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsolePortsId(id, callback)</td>
    <td style="padding:15px">dcim_console-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsolePortsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termination).</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-ports/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPortTemplates(id, name, type, q, devicetypeId, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, devicetypeIdN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Console Server Port Templates</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimConsoleServerPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPortTemplatesId(id, callback)</td>
    <td style="padding:15px">get DCIM Console Server Port Templates by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsoleServerPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsoleServerPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsoleServerPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_console-server-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPorts(id, name, description, connectionStatus, q, regionId, region, siteId, site, deviceId, device, tag, type, cabled, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, connectionStatusN, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, typeN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Console Server Ports</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimConsoleServerPorts(data, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPortsId(id, callback)</td>
    <td style="padding:15px">get DCIM Console Server Ports by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimConsoleServerPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimConsoleServerPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimConsoleServerPortsId(id, callback)</td>
    <td style="padding:15px">dcim_console-server-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimConsoleServerPortsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termination).</td>
    <td style="padding:15px">{base_path}/{version}/dcim/console-server-ports/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceBayTemplates(id, name, q, devicetypeId, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, devicetypeIdN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Device Bay Templates</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDeviceBayTemplates(data, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceBayTemplatesId(id, callback)</td>
    <td style="padding:15px">get DCIM Device Bay Templates by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceBayTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceBayTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceBayTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_device-bay-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bay-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceBays(id, name, description, q, regionId, region, siteId, site, deviceId, device, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Device Bays</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDeviceBays(data, callback)</td>
    <td style="padding:15px">dcim_device-bays_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceBaysId(id, callback)</td>
    <td style="padding:15px">get DCIM Device Bays by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceBaysId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-bays_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceBaysId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-bays_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceBaysId(id, callback)</td>
    <td style="padding:15px">dcim_device-bays_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-bays/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceRoles(id, name, slug, color, vmRole, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, colorN, colorIc, colorNic, colorIew, colorNiew, colorIsw, colorNisw, colorIe, colorNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Device Roles</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDeviceRoles(data, callback)</td>
    <td style="padding:15px">dcim_device-roles_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceRolesId(id, callback)</td>
    <td style="padding:15px">get DCIM Device Roles by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceRolesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-roles_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceRolesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-roles_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceRolesId(id, callback)</td>
    <td style="padding:15px">dcim_device-roles_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceTypes(id, model, slug, partNumber, uHeight, isFullDepth, subdeviceRole, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, manufacturerId, manufacturer, consolePorts, consoleServerPorts, powerPorts, powerOutlets, interfaces, passThroughPorts, deviceBays, tag, idN, idLte, idLt, idGte, idGt, modelN, modelIc, modelNic, modelIew, modelNiew, modelIsw, modelNisw, modelIe, modelNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, partNumberN, partNumberIc, partNumberNic, partNumberIew, partNumberNiew, partNumberIsw, partNumberNisw, partNumberIe, partNumberNie, uHeightN, uHeightLte, uHeightLt, uHeightGte, uHeightGt, subdeviceRoleN, manufacturerIdN, manufacturerN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Device Types</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDeviceTypes(data, callback)</td>
    <td style="padding:15px">dcim_device-types_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDeviceTypesId(id, callback)</td>
    <td style="padding:15px">get DCIM Device Types by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDeviceTypesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-types_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDeviceTypesId(id, data, callback)</td>
    <td style="padding:15px">dcim_device-types_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDeviceTypesId(id, callback)</td>
    <td style="padding:15px">dcim_device-types_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/device-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDevices(id, name, assetTag, face, position, vcPosition, vcPriority, tenantGroupId, tenantGroup, tenantId, tenant, localContextData, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, manufacturerId, manufacturer, deviceTypeId, roleId, role, platformId, platform, regionId, region, siteId, site, rackGroupId, rackId, clusterId, model, status, isFullDepth, macAddress, serial, hasPrimaryIp, virtualChassisId, virtualChassisMember, consolePorts, consoleServerPorts, powerPorts, powerOutlets, interfaces, passThroughPorts, deviceBays, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, assetTagN, assetTagIc, assetTagNic, assetTagIew, assetTagNiew, assetTagIsw, assetTagNisw, assetTagIe, assetTagNie, faceN, positionN, positionLte, positionLt, positionGte, positionGt, vcPositionN, vcPositionLte, vcPositionLt, vcPositionGte, vcPositionGt, vcPriorityN, vcPriorityLte, vcPriorityLt, vcPriorityGte, vcPriorityGt, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, manufacturerIdN, manufacturerN, deviceTypeIdN, roleIdN, roleN, platformIdN, platformN, regionIdN, regionN, siteIdN, siteN, rackGroupIdN, rackIdN, clusterIdN, modelN, statusN, macAddressN, macAddressIc, macAddressNic, macAddressIew, macAddressNiew, macAddressIsw, macAddressNisw, macAddressIe, macAddressNie, virtualChassisIdN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Devices</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimDevices(data, callback)</td>
    <td style="padding:15px">dcim_devices_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDevicesId(id, callback)</td>
    <td style="padding:15px">get DCIM Devices by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimDevicesId(id, data, callback)</td>
    <td style="padding:15px">dcim_devices_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimDevicesId(id, data, callback)</td>
    <td style="padding:15px">dcim_devices_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimDevicesId(id, callback)</td>
    <td style="padding:15px">dcim_devices_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDevicesIdGraphs(id, callback)</td>
    <td style="padding:15px">A convenience method for rendering graphs for a particular Device.</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/graphs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimDevicesIdNapalm(id, method, callback)</td>
    <td style="padding:15px">Execute a NAPALM method on a Device</td>
    <td style="padding:15px">{base_path}/{version}/dcim/devices/{pathv1}/napalm/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimFrontPortTemplates(id, name, type, q, devicetypeId, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, devicetypeIdN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Front Port Templates</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimFrontPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimFrontPortTemplatesId(id, callback)</td>
    <td style="padding:15px">get DCIM Front Port Templates by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimFrontPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimFrontPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimFrontPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_front-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimFrontPorts(id, name, type, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Front Ports</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimFrontPorts(data, callback)</td>
    <td style="padding:15px">dcim_front-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimFrontPortsId(id, callback)</td>
    <td style="padding:15px">get DCIM Front Ports by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimFrontPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_front-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimFrontPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_front-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimFrontPortsId(id, callback)</td>
    <td style="padding:15px">dcim_front-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/front-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfaceConnections(connectionStatus, site, deviceId, device, connectionStatusN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">dcim_interface-connections_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-connections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfaceTemplates(id, name, type, mgmtOnly, q, devicetypeId, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, devicetypeIdN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Interface Templates</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimInterfaceTemplates(data, callback)</td>
    <td style="padding:15px">dcim_interface-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfaceTemplatesId(id, callback)</td>
    <td style="padding:15px">get DCIM Interface Templates by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimInterfaceTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_interface-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimInterfaceTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_interface-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimInterfaceTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_interface-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interface-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfaces(id, name, connectionStatus, type, enabled, mtu, mgmtOnly, mode, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, kind, lagId, macAddress, vlanId, vlan, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, connectionStatusN, typeN, mtuN, mtuLte, mtuLt, mtuGte, mtuGt, modeN, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, tagN, lagIdN, macAddressN, macAddressIc, macAddressNic, macAddressIew, macAddressNiew, macAddressIsw, macAddressNisw, macAddressIe, macAddressNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimInterfaces(data, callback)</td>
    <td style="padding:15px">dcim_interfaces_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfacesId(id, callback)</td>
    <td style="padding:15px">get DCIM Interfaces by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimInterfacesId(id, data, callback)</td>
    <td style="padding:15px">dcim_interfaces_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimInterfacesId(id, data, callback)</td>
    <td style="padding:15px">dcim_interfaces_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimInterfacesId(id, callback)</td>
    <td style="padding:15px">dcim_interfaces_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfacesIdGraphs(id, callback)</td>
    <td style="padding:15px">A convenience method for rendering graphs for a particular interface.</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/graphs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInterfacesIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termination).</td>
    <td style="padding:15px">{base_path}/{version}/dcim/interfaces/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInventoryItems(id, name, partId, assetTag, discovered, q, regionId, region, siteId, site, deviceId, device, tag, parentId, manufacturerId, manufacturer, serial, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, partIdN, partIdIc, partIdNic, partIdIew, partIdNiew, partIdIsw, partIdNisw, partIdIe, partIdNie, assetTagN, assetTagIc, assetTagNic, assetTagIew, assetTagNiew, assetTagIsw, assetTagNisw, assetTagIe, assetTagNie, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, parentIdN, manufacturerIdN, manufacturerN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Inventory Items</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimInventoryItems(data, callback)</td>
    <td style="padding:15px">dcim_inventory-items_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimInventoryItemsId(id, callback)</td>
    <td style="padding:15px">get DCIM Inventory Items by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimInventoryItemsId(id, data, callback)</td>
    <td style="padding:15px">dcim_inventory-items_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimInventoryItemsId(id, data, callback)</td>
    <td style="padding:15px">dcim_inventory-items_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimInventoryItemsId(id, callback)</td>
    <td style="padding:15px">dcim_inventory-items_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/inventory-items/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimManufacturers(id, name, slug, description, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Manufacturers</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimManufacturers(data, callback)</td>
    <td style="padding:15px">dcim_manufacturers_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimManufacturersId(id, callback)</td>
    <td style="padding:15px">get DCIM Manufacturers by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimManufacturersId(id, data, callback)</td>
    <td style="padding:15px">dcim_manufacturers_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimManufacturersId(id, data, callback)</td>
    <td style="padding:15px">dcim_manufacturers_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimManufacturersId(id, callback)</td>
    <td style="padding:15px">dcim_manufacturers_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/manufacturers/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPlatforms(id, name, slug, napalmDriver, description, q, manufacturerId, manufacturer, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, napalmDriverN, napalmDriverIc, napalmDriverNic, napalmDriverIew, napalmDriverNiew, napalmDriverIsw, napalmDriverNisw, napalmDriverIe, napalmDriverNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, manufacturerIdN, manufacturerN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Platforms</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPlatforms(data, callback)</td>
    <td style="padding:15px">dcim_platforms_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPlatformsId(id, callback)</td>
    <td style="padding:15px">get DCIM Platforms by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPlatformsId(id, data, callback)</td>
    <td style="padding:15px">dcim_platforms_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPlatformsId(id, data, callback)</td>
    <td style="padding:15px">dcim_platforms_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPlatformsId(id, callback)</td>
    <td style="padding:15px">dcim_platforms_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/platforms/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerConnections(name, connectionStatus, site, deviceId, device, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, connectionStatusN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">dcim_power-connections_list</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-connections/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerFeeds(id, name, status, type, supply, phase, voltage, amperage, maxUtilization, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, powerPanelId, rackId, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, statusN, typeN, supplyN, phaseN, voltageN, voltageLte, voltageLt, voltageGte, voltageGt, amperageN, amperageLte, amperageLt, amperageGte, amperageGt, maxUtilizationN, maxUtilizationLte, maxUtilizationLt, maxUtilizationGte, maxUtilizationGt, regionIdN, regionN, siteIdN, siteN, powerPanelIdN, rackIdN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Power Feeds</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerFeeds(data, callback)</td>
    <td style="padding:15px">dcim_power-feeds_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerFeedsId(id, callback)</td>
    <td style="padding:15px">get DCIM Power Feeds by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerFeedsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-feeds_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerFeedsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-feeds_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerFeedsId(id, callback)</td>
    <td style="padding:15px">dcim_power-feeds_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-feeds/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutletTemplates(id, name, type, feedLeg, q, devicetypeId, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, feedLegN, devicetypeIdN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Power Outlet Templates</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerOutletTemplates(data, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutletTemplatesId(id, callback)</td>
    <td style="padding:15px">get DCIM Power Outlet Templates by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerOutletTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerOutletTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerOutletTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_power-outlet-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlet-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutlets(id, name, feedLeg, description, connectionStatus, q, regionId, region, siteId, site, deviceId, device, tag, type, cabled, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, feedLegN, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, connectionStatusN, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, typeN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Power Outlets</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerOutlets(data, callback)</td>
    <td style="padding:15px">dcim_power-outlets_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutletsId(id, callback)</td>
    <td style="padding:15px">get DCIM Power Outlets by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerOutletsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-outlets_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerOutletsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-outlets_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerOutletsId(id, callback)</td>
    <td style="padding:15px">dcim_power-outlets_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerOutletsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termination).</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-outlets/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPanels(id, name, q, regionId, region, siteId, site, rackGroupId, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, regionIdN, regionN, siteIdN, siteN, rackGroupIdN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Power Panels</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerPanels(data, callback)</td>
    <td style="padding:15px">dcim_power-panels_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPanelsId(id, callback)</td>
    <td style="padding:15px">get DCIM Power Panels by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerPanelsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-panels_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerPanelsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-panels_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerPanelsId(id, callback)</td>
    <td style="padding:15px">dcim_power-panels_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-panels/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPortTemplates(id, name, type, maximumDraw, allocatedDraw, q, devicetypeId, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, maximumDrawN, maximumDrawLte, maximumDrawLt, maximumDrawGte, maximumDrawGt, allocatedDrawN, allocatedDrawLte, allocatedDrawLt, allocatedDrawGte, allocatedDrawGt, devicetypeIdN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Power Port Templates</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPortTemplatesId(id, callback)</td>
    <td style="padding:15px">get DCIM Power Port Templates by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_power-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPorts(id, name, maximumDraw, allocatedDraw, description, connectionStatus, q, regionId, region, siteId, site, deviceId, device, tag, type, cabled, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, maximumDrawN, maximumDrawLte, maximumDrawLt, maximumDrawGte, maximumDrawGt, allocatedDrawN, allocatedDrawLte, allocatedDrawLt, allocatedDrawGte, allocatedDrawGt, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, connectionStatusN, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, typeN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Power Ports</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimPowerPorts(data, callback)</td>
    <td style="padding:15px">dcim_power-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPortsId(id, callback)</td>
    <td style="padding:15px">get DCIM Power Ports by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimPowerPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimPowerPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_power-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimPowerPortsId(id, callback)</td>
    <td style="padding:15px">dcim_power-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimPowerPortsIdTrace(id, callback)</td>
    <td style="padding:15px">Trace a complete cable path and return each segment as a three-tuple of (termination, cable, termination).</td>
    <td style="padding:15px">{base_path}/{version}/dcim/power-ports/{pathv1}/trace/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackGroups(id, name, slug, description, q, regionId, region, siteId, site, parentId, parent, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, parentIdN, parentN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Rack Groups</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRackGroups(data, callback)</td>
    <td style="padding:15px">dcim_rack-groups_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackGroupsId(id, callback)</td>
    <td style="padding:15px">get DCIM Rack Groups by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRackGroupsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-groups_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRackGroupsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-groups_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRackGroupsId(id, callback)</td>
    <td style="padding:15px">dcim_rack-groups_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackReservations(id, created, tenantGroupId, tenantGroup, tenantId, tenant, q, rackId, siteId, site, groupId, group, userId, user, idN, idLte, idLt, idGte, idGt, createdN, createdLte, createdLt, createdGte, createdGt, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, rackIdN, siteIdN, siteN, groupIdN, groupN, userIdN, userN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Rack Reservations</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRackReservations(data, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackReservationsId(id, callback)</td>
    <td style="padding:15px">get DCIM Rack Reservations by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRackReservationsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRackReservationsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRackReservationsId(id, callback)</td>
    <td style="padding:15px">dcim_rack-reservations_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-reservations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackRoles(id, name, slug, color, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, colorN, colorIc, colorNic, colorIew, colorNiew, colorIsw, colorNisw, colorIe, colorNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Rack Roles</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRackRoles(data, callback)</td>
    <td style="padding:15px">dcim_rack-roles_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRackRolesId(id, callback)</td>
    <td style="padding:15px">get DCIM Rack Roles by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRackRolesId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-roles_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRackRolesId(id, data, callback)</td>
    <td style="padding:15px">dcim_rack-roles_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRackRolesId(id, callback)</td>
    <td style="padding:15px">dcim_rack-roles_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rack-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRacks(id, name, facilityId, assetTag, type, width, uHeight, descUnits, outerWidth, outerDepth, outerUnit, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, groupId, group, status, roleId, role, serial, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, facilityIdN, facilityIdIc, facilityIdNic, facilityIdIew, facilityIdNiew, facilityIdIsw, facilityIdNisw, facilityIdIe, facilityIdNie, assetTagN, assetTagIc, assetTagNic, assetTagIew, assetTagNiew, assetTagIsw, assetTagNisw, assetTagIe, assetTagNie, typeN, widthN, uHeightN, uHeightLte, uHeightLt, uHeightGte, uHeightGt, outerWidthN, outerWidthLte, outerWidthLt, outerWidthGte, outerWidthGt, outerDepthN, outerDepthLte, outerDepthLt, outerDepthGte, outerDepthGt, outerUnitN, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, regionIdN, regionN, siteIdN, siteN, groupIdN, groupN, statusN, roleIdN, roleN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Racks</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRacks(data, callback)</td>
    <td style="padding:15px">dcim_racks_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRacksId(id, callback)</td>
    <td style="padding:15px">get DCIM Racks by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRacksId(id, data, callback)</td>
    <td style="padding:15px">dcim_racks_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRacksId(id, data, callback)</td>
    <td style="padding:15px">dcim_racks_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRacksId(id, callback)</td>
    <td style="padding:15px">dcim_racks_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRacksIdElevation(id, q, face = 'front', render = 'json', unitWidth, unitHeight, legendWidth, exclude, expandDevices, includeImages, customFields, callback)</td>
    <td style="padding:15px">Rack elevation representing the list of rack units. Also supports rendering the elevation as an SVG.</td>
    <td style="padding:15px">{base_path}/{version}/dcim/racks/{pathv1}/elevation/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRearPortTemplates(id, name, type, positions, q, devicetypeId, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, positionsN, positionsLte, positionsLt, positionsGte, positionsGt, devicetypeIdN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Rear Port Templates</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRearPortTemplates(data, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRearPortTemplatesId(id, callback)</td>
    <td style="padding:15px">get DCIM Rear Port Templates by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRearPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRearPortTemplatesId(id, data, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRearPortTemplatesId(id, callback)</td>
    <td style="padding:15px">dcim_rear-port-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-port-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRearPorts(id, name, type, positions, description, q, regionId, region, siteId, site, deviceId, device, tag, cabled, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, typeN, positionsN, positionsLte, positionsLt, positionsGte, positionsGt, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, deviceIdN, deviceN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Rear Ports</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRearPorts(data, callback)</td>
    <td style="padding:15px">dcim_rear-ports_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRearPortsId(id, callback)</td>
    <td style="padding:15px">get DCIM Rear Ports by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRearPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rear-ports_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRearPortsId(id, data, callback)</td>
    <td style="padding:15px">dcim_rear-ports_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRearPortsId(id, callback)</td>
    <td style="padding:15px">dcim_rear-ports_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/rear-ports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRegions(id, name, slug, description, q, parentId, parent, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, parentIdN, parentN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Regions</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimRegions(data, callback)</td>
    <td style="padding:15px">dcim_regions_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimRegionsId(id, callback)</td>
    <td style="padding:15px">get DCIM Regions by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimRegionsId(id, data, callback)</td>
    <td style="padding:15px">dcim_regions_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimRegionsId(id, data, callback)</td>
    <td style="padding:15px">dcim_regions_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimRegionsId(id, callback)</td>
    <td style="padding:15px">dcim_regions_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/regions/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimSites(id, name, slug, facility, asn, latitude, longitude, contactName, contactPhone, contactEmail, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, status, regionId, region, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, facilityN, facilityIc, facilityNic, facilityIew, facilityNiew, facilityIsw, facilityNisw, facilityIe, facilityNie, asnN, asnLte, asnLt, asnGte, asnGt, latitudeN, latitudeLte, latitudeLt, latitudeGte, latitudeGt, longitudeN, longitudeLte, longitudeLt, longitudeGte, longitudeGt, contactNameN, contactNameIc, contactNameNic, contactNameIew, contactNameNiew, contactNameIsw, contactNameNisw, contactNameIe, contactNameNie, contactPhoneN, contactPhoneIc, contactPhoneNic, contactPhoneIew, contactPhoneNiew, contactPhoneIsw, contactPhoneNisw, contactPhoneIe, contactPhoneNie, contactEmailN, contactEmailIc, contactEmailNic, contactEmailIew, contactEmailNiew, contactEmailIsw, contactEmailNisw, contactEmailIe, contactEmailNie, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, statusN, regionIdN, regionN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Sites</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimSites(data, callback)</td>
    <td style="padding:15px">dcim_sites_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimSitesId(id, callback)</td>
    <td style="padding:15px">get DCIM Sites by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimSitesId(id, data, callback)</td>
    <td style="padding:15px">dcim_sites_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimSitesId(id, data, callback)</td>
    <td style="padding:15px">dcim_sites_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimSitesId(id, callback)</td>
    <td style="padding:15px">dcim_sites_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimSitesIdGraphs(id, callback)</td>
    <td style="padding:15px">A convenience method for rendering graphs for a particular site.</td>
    <td style="padding:15px">{base_path}/{version}/dcim/sites/{pathv1}/graphs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimVirtualChassis(id, domain, q, regionId, region, siteId, site, tenantId, tenant, tag, idN, idLte, idLt, idGte, idGt, domainN, domainIc, domainNic, domainIew, domainNiew, domainIsw, domainNisw, domainIe, domainNie, regionIdN, regionN, siteIdN, siteN, tenantIdN, tenantN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get DCIM Virtual Chassis</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimVirtualChassis(data, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimVirtualChassisId(id, callback)</td>
    <td style="padding:15px">get DCIM Virtual Chassis by id</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimVirtualChassisId(id, data, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimVirtualChassisId(id, data, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimVirtualChassisId(id, callback)</td>
    <td style="padding:15px">dcim_virtual-chassis_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/virtual-chassis/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasCustomFieldChoices(callback)</td>
    <td style="padding:15px">extras__custom_field_choices_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/_custom_field_choices/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasCustomFieldChoicesId(id, callback)</td>
    <td style="padding:15px">extras__custom_field_choices_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/_custom_field_choices/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasConfigContexts(id, name, isActive, q, regionId, region, siteId, site, roleId, role, platformId, platform, clusterGroupId, clusterGroup, clusterId, tenantGroupId, tenantGroup, tenantId, tenant, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, regionIdN, regionN, siteIdN, siteN, roleIdN, roleN, platformIdN, platformN, clusterGroupIdN, clusterGroupN, clusterIdN, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Extra Config Contexts</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasConfigContexts(data, callback)</td>
    <td style="padding:15px">extras_config-contexts_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasConfigContextsId(id, callback)</td>
    <td style="padding:15px">get Extra Config Contexts by id</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasConfigContextsId(id, data, callback)</td>
    <td style="padding:15px">extras_config-contexts_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasConfigContextsId(id, data, callback)</td>
    <td style="padding:15px">extras_config-contexts_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasConfigContextsId(id, callback)</td>
    <td style="padding:15px">extras_config-contexts_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/config-contexts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasExportTemplates(id, contentType, name, templateLanguage, idN, idLte, idLt, idGte, idGt, contentTypeN, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, templateLanguageN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Extra Export Templates</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasExportTemplates(data, callback)</td>
    <td style="padding:15px">extras_export-templates_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasExportTemplatesId(id, callback)</td>
    <td style="padding:15px">get Extra Export Templates by id</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasExportTemplatesId(id, data, callback)</td>
    <td style="padding:15px">extras_export-templates_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasExportTemplatesId(id, data, callback)</td>
    <td style="padding:15px">extras_export-templates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasExportTemplatesId(id, callback)</td>
    <td style="padding:15px">extras_export-templates_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/export-templates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasGraphs(id, type, name, templateLanguage, idN, idLte, idLt, idGte, idGt, typeN, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, templateLanguageN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Extra Graphs</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasGraphs(data, callback)</td>
    <td style="padding:15px">extras_graphs_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasGraphsId(id, callback)</td>
    <td style="padding:15px">get Extra Graphs by id</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasGraphsId(id, data, callback)</td>
    <td style="padding:15px">extras_graphs_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasGraphsId(id, data, callback)</td>
    <td style="padding:15px">extras_graphs_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasGraphsId(id, callback)</td>
    <td style="padding:15px">extras_graphs_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/graphs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasImageAttachments(limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Extra Image Attachments</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasImageAttachments(data, callback)</td>
    <td style="padding:15px">extras_image-attachments_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasImageAttachmentsId(id, callback)</td>
    <td style="padding:15px">get Extra Image Attachments by id</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasImageAttachmentsId(id, data, callback)</td>
    <td style="padding:15px">extras_image-attachments_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasImageAttachmentsId(id, data, callback)</td>
    <td style="padding:15px">extras_image-attachments_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasImageAttachmentsId(id, callback)</td>
    <td style="padding:15px">extras_image-attachments_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/image-attachments/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasObjectChanges(id, user, userName, requestId, action, changedObjectType, changedObjectId, objectRepr, q, time, idN, idLte, idLt, idGte, idGt, userN, userNameN, userNameIc, userNameNic, userNameIew, userNameNiew, userNameIsw, userNameNisw, userNameIe, userNameNie, actionN, changedObjectTypeN, changedObjectIdN, changedObjectIdLte, changedObjectIdLt, changedObjectIdGte, changedObjectIdGt, objectReprN, objectReprIc, objectReprNic, objectReprIew, objectReprNiew, objectReprIsw, objectReprNisw, objectReprIe, objectReprNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">Retrieve a list of recent changes.</td>
    <td style="padding:15px">{base_path}/{version}/extras/object-changes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasObjectChangesId(id, callback)</td>
    <td style="padding:15px">Retrieve a list of recent changes.</td>
    <td style="padding:15px">{base_path}/{version}/extras/object-changes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasReports(callback)</td>
    <td style="padding:15px">Compile all reports and their related results (if any). Result data is deferred in the list view.</td>
    <td style="padding:15px">{base_path}/{version}/extras/reports/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasReportsId(id, callback)</td>
    <td style="padding:15px">Retrieve a single Report identified as " . ".</td>
    <td style="padding:15px">{base_path}/{version}/extras/reports/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasReportsIdRun(id, callback)</td>
    <td style="padding:15px">Run a Report and create a new ReportResult, overwriting any previous result for the Report.</td>
    <td style="padding:15px">{base_path}/{version}/extras/reports/{pathv1}/run/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasScripts(callback)</td>
    <td style="padding:15px">extras_scripts_list</td>
    <td style="padding:15px">{base_path}/{version}/extras/scripts/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasScriptsId(id, callback)</td>
    <td style="padding:15px">extras_scripts_read</td>
    <td style="padding:15px">{base_path}/{version}/extras/scripts/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasTags(id, name, slug, color, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, colorN, colorIc, colorNic, colorIew, colorNiew, colorIsw, colorNisw, colorIe, colorNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Extra Tags</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExtrasTags(data, callback)</td>
    <td style="padding:15px">extras_tags_create</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExtrasTagsId(id, callback)</td>
    <td style="padding:15px">get Extra Tags by id</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putExtrasTagsId(id, data, callback)</td>
    <td style="padding:15px">extras_tags_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchExtrasTagsId(id, data, callback)</td>
    <td style="padding:15px">extras_tags_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExtrasTagsId(id, callback)</td>
    <td style="padding:15px">extras_tags_delete</td>
    <td style="padding:15px">{base_path}/{version}/extras/tags/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAggregates(id, dateAdded, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, family, prefix, rirId, rir, tag, idN, idLte, idLt, idGte, idGt, dateAddedN, dateAddedLte, dateAddedLt, dateAddedGte, dateAddedGt, rirIdN, rirN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get IPAM Aggregates</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamAggregates(data, callback)</td>
    <td style="padding:15px">ipam_aggregates_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAggregatesId(id, callback)</td>
    <td style="padding:15px">get IPAM Aggregates by id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamAggregatesId(id, data, callback)</td>
    <td style="padding:15px">ipam_aggregates_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamAggregatesId(id, data, callback)</td>
    <td style="padding:15px">ipam_aggregates_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamAggregatesId(id, callback)</td>
    <td style="padding:15px">ipam_aggregates_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/aggregates/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamIpAddresses(id, dnsName, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, family, parent, address, maskLength, vrfId, vrf, device, deviceId, virtualMachineId, virtualMachine, interfaceParam, interfaceId, assignedToInterface, status, role, tag, idN, idLte, idLt, idGte, idGt, dnsNameN, dnsNameIc, dnsNameNic, dnsNameIew, dnsNameNiew, dnsNameIsw, dnsNameNisw, dnsNameIe, dnsNameNie, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, vrfIdN, vrfN, virtualMachineIdN, virtualMachineN, interfaceN, interfaceIdN, statusN, roleN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get IPAM IP Addresses</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamIpAddresses(data, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamIpAddressesId(id, callback)</td>
    <td style="padding:15px">get IPAM IP Addresses by id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamIpAddressesId(id, data, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamIpAddressesId(id, data, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamIpAddressesId(id, callback)</td>
    <td style="padding:15px">ipam_ip-addresses_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/ip-addresses/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPrefixes(id, isPool, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, family, prefix, within, withinInclude, contains, maskLength, vrfId, vrf, regionId, region, siteId, site, vlanId, vlanVid, roleId, role, status, tag, idN, idLte, idLt, idGte, idGt, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, vrfIdN, vrfN, regionIdN, regionN, siteIdN, siteN, vlanIdN, roleIdN, roleN, statusN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get IPAM Prefixes</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamPrefixes(data, callback)</td>
    <td style="padding:15px">ipam_prefixes_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPrefixesId(id, callback)</td>
    <td style="padding:15px">get IPAM Prefixes by id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamPrefixesId(id, data, callback)</td>
    <td style="padding:15px">ipam_prefixes_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamPrefixesId(id, data, callback)</td>
    <td style="padding:15px">ipam_prefixes_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamPrefixesId(id, callback)</td>
    <td style="padding:15px">ipam_prefixes_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPrefixesIdAvailableIps(id, callback)</td>
    <td style="padding:15px">A convenience method for returning available IP addresses within a prefix. By default, the number of IPs
returned will be equivalent to PAGINATE_COUNT. An arbitrary limit (up to MAX_PAGE_SIZE, if set) may be passed,
however results will not be paginated.

The advisory lock decorator uses a PostgreSQL advisory lock to prevent this API from being
invoked in parallel, which results in a race condition where multiple insertions can occur.</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/available-ips/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamPrefixesIdAvailableIps(id, data, callback)</td>
    <td style="padding:15px">A convenience method for returning available IP addresses within a prefix. By default, the number of IPs
returned will be equivalent to PAGINATE_COUNT. An arbitrary limit (up to MAX_PAGE_SIZE, if set) may be passed,
however results will not be paginated.

The advisory lock decorator uses a PostgreSQL advisory lock to prevent this API from being
invoked in parallel, which results in a race condition where multiple insertions can occur.</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/available-ips/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamPrefixesIdAvailablePrefixes(id, callback)</td>
    <td style="padding:15px">A convenience method for returning available child prefixes within a parent.</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/available-prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamPrefixesIdAvailablePrefixes(id, data, callback)</td>
    <td style="padding:15px">A convenience method for returning available child prefixes within a parent.</td>
    <td style="padding:15px">{base_path}/{version}/ipam/prefixes/{pathv1}/available-prefixes/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRirs(id, name, slug, isPrivate, description, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get IPAM RIRS</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamRirs(data, callback)</td>
    <td style="padding:15px">ipam_rirs_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRirsId(id, callback)</td>
    <td style="padding:15px">get IPAM RIRS by id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamRirsId(id, data, callback)</td>
    <td style="padding:15px">ipam_rirs_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamRirsId(id, data, callback)</td>
    <td style="padding:15px">ipam_rirs_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamRirsId(id, callback)</td>
    <td style="padding:15px">ipam_rirs_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rirs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRoles(id, name, slug, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get IPAM Roles</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamRoles(data, callback)</td>
    <td style="padding:15px">ipam_roles_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRolesId(id, callback)</td>
    <td style="padding:15px">get IPAM Roles by id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamRolesId(id, data, callback)</td>
    <td style="padding:15px">ipam_roles_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamRolesId(id, data, callback)</td>
    <td style="padding:15px">ipam_roles_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamRolesId(id, callback)</td>
    <td style="padding:15px">ipam_roles_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamServices(id, name, protocol, port, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, deviceId, device, virtualMachineId, virtualMachine, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, protocolN, portN, portLte, portLt, portGte, portGt, deviceIdN, deviceN, virtualMachineIdN, virtualMachineN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get IPAM Services</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamServices(data, callback)</td>
    <td style="padding:15px">ipam_services_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamServicesId(id, callback)</td>
    <td style="padding:15px">get IPAM Services by id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamServicesId(id, data, callback)</td>
    <td style="padding:15px">ipam_services_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamServicesId(id, data, callback)</td>
    <td style="padding:15px">ipam_services_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamServicesId(id, callback)</td>
    <td style="padding:15px">ipam_services_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/services/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVlanGroups(id, name, slug, description, q, regionId, region, siteId, site, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, regionIdN, regionN, siteIdN, siteN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get IPAM Vlan Groups</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamVlanGroups(data, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVlanGroupsId(id, callback)</td>
    <td style="padding:15px">get IPAM Vlan Groups by id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamVlanGroupsId(id, data, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamVlanGroupsId(id, data, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamVlanGroupsId(id, callback)</td>
    <td style="padding:15px">ipam_vlan-groups_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlan-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVlans(id, vid, name, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, groupId, group, roleId, role, status, tag, idN, idLte, idLt, idGte, idGt, vidN, vidLte, vidLt, vidGte, vidGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, regionIdN, regionN, siteIdN, siteN, groupIdN, groupN, roleIdN, roleN, statusN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get IPAM Vlans</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamVlans(data, callback)</td>
    <td style="padding:15px">ipam_vlans_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVlansId(id, callback)</td>
    <td style="padding:15px">get IPAM Vlans by id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamVlansId(id, data, callback)</td>
    <td style="padding:15px">ipam_vlans_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamVlansId(id, data, callback)</td>
    <td style="padding:15px">ipam_vlans_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamVlansId(id, callback)</td>
    <td style="padding:15px">ipam_vlans_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVrfs(id, name, rd, enforceUnique, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, rdN, rdIc, rdNic, rdIew, rdNiew, rdIsw, rdNisw, rdIe, rdNie, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get IPAM VRFS</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamVrfs(data, callback)</td>
    <td style="padding:15px">ipam_vrfs_create</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamVrfsId(id, callback)</td>
    <td style="padding:15px">get IPAM VRFS by id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putIpamVrfsId(id, data, callback)</td>
    <td style="padding:15px">ipam_vrfs_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamVrfsId(id, data, callback)</td>
    <td style="padding:15px">ipam_vrfs_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamVrfsId(id, callback)</td>
    <td style="padding:15px">ipam_vrfs_delete</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vrfs/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretsGenerateRsaKeyPair(callback)</td>
    <td style="padding:15px">This endpoint can be used to generate a new RSA key pair. The keys are returned in PEM format.</td>
    <td style="padding:15px">{base_path}/{version}/secrets/generate-rsa-key-pair/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretsGetSessionKey(callback)</td>
    <td style="padding:15px">Retrieve a temporary session key to use for encrypting and decrypting secrets via the API. The user's private RSA
key is POSTed with the name `private_key`. An example:

    curl -v -X POST -H "Authorization: Token  " -H "Accept: application/json; indent=4" \
    --data-urlencode "private_key@ " https://netbox/api/secrets/get-session-key/

This request will yield a base64-encoded session key to be included in an `X-Session-Key` header in future requests:

    {
        "session_key": "+8t4SI6Xik...(description truncated)</td>
    <td style="padding:15px">{base_path}/{version}/secrets/get-session-key/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretsSecretRoles(id, name, slug, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Secret Roles</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secret-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretsSecretRoles(data, callback)</td>
    <td style="padding:15px">secrets_secret-roles_create</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secret-roles/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretsSecretRolesId(id, callback)</td>
    <td style="padding:15px">get Secret Roles by id</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secret-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSecretsSecretRolesId(id, data, callback)</td>
    <td style="padding:15px">secrets_secret-roles_update</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secret-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSecretsSecretRolesId(id, data, callback)</td>
    <td style="padding:15px">secrets_secret-roles_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secret-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretsSecretRolesId(id, callback)</td>
    <td style="padding:15px">secrets_secret-roles_delete</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secret-roles/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretsSecrets(id, name, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, roleId, role, deviceId, device, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, roleIdN, roleN, deviceIdN, deviceN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">secrets_secrets_list</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secrets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretsSecrets(data, callback)</td>
    <td style="padding:15px">secrets_secrets_create</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secrets/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretsSecretsId(id, callback)</td>
    <td style="padding:15px">secrets_secrets_read</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secrets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSecretsSecretsId(id, data, callback)</td>
    <td style="padding:15px">secrets_secrets_update</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secrets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchSecretsSecretsId(id, data, callback)</td>
    <td style="padding:15px">secrets_secrets_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secrets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretsSecretsId(id, callback)</td>
    <td style="padding:15px">secrets_secrets_delete</td>
    <td style="padding:15px">{base_path}/{version}/secrets/secrets/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenancyTenantGroups(id, name, slug, description, q, parentId, parent, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, parentIdN, parentN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Tenancy Groups</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTenancyTenantGroups(data, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_create</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenancyTenantGroupsId(id, callback)</td>
    <td style="padding:15px">get Tenancy Groups by id</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTenancyTenantGroupsId(id, data, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTenancyTenantGroupsId(id, data, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenancyTenantGroupsId(id, callback)</td>
    <td style="padding:15px">tenancy_tenant-groups_delete</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenant-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenancyTenants(id, name, slug, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, groupId, group, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, groupIdN, groupN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Tenancy</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTenancyTenants(data, callback)</td>
    <td style="padding:15px">tenancy_tenants_create</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTenancyTenantsId(id, callback)</td>
    <td style="padding:15px">get Tenancy by id</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTenancyTenantsId(id, data, callback)</td>
    <td style="padding:15px">tenancy_tenants_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchTenancyTenantsId(id, data, callback)</td>
    <td style="padding:15px">tenancy_tenants_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTenancyTenantsId(id, callback)</td>
    <td style="padding:15px">tenancy_tenants_delete</td>
    <td style="padding:15px">{base_path}/{version}/tenancy/tenants/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusterGroups(id, name, slug, description, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Virtualization Cluster Groups</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationClusterGroups(data, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusterGroupsId(id, callback)</td>
    <td style="padding:15px">get Virtualization Cluster Groups by id</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationClusterGroupsId(id, data, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationClusterGroupsId(id, data, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationClusterGroupsId(id, callback)</td>
    <td style="padding:15px">virtualization_cluster-groups_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-groups/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusterTypes(id, name, slug, description, q, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Virtualization Cluster Types</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationClusterTypes(data, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusterTypesId(id, callback)</td>
    <td style="padding:15px">get Virtualization Cluster Types by id</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationClusterTypesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationClusterTypesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationClusterTypesId(id, callback)</td>
    <td style="padding:15px">virtualization_cluster-types_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/cluster-types/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClusters(id, name, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, regionId, region, siteId, site, groupId, group, typeId, type, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, regionIdN, regionN, siteIdN, siteN, groupIdN, groupN, typeIdN, typeN, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Virtualization Clusters</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationClusters(data, callback)</td>
    <td style="padding:15px">virtualization_clusters_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationClustersId(id, callback)</td>
    <td style="padding:15px">get Virtualization Clusters by id</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationClustersId(id, data, callback)</td>
    <td style="padding:15px">virtualization_clusters_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationClustersId(id, data, callback)</td>
    <td style="padding:15px">virtualization_clusters_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationClustersId(id, callback)</td>
    <td style="padding:15px">virtualization_clusters_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/clusters/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationInterfaces(id, name, enabled, mtu, q, virtualMachineId, virtualMachine, macAddress, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, mtuN, mtuLte, mtuLt, mtuGte, mtuGt, virtualMachineIdN, virtualMachineN, macAddressN, macAddressIc, macAddressNic, macAddressIew, macAddressNiew, macAddressIsw, macAddressNisw, macAddressIe, macAddressNie, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Virtualization Interfaces</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationInterfaces(data, callback)</td>
    <td style="padding:15px">virtualization_interfaces_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationInterfacesId(id, callback)</td>
    <td style="padding:15px">get Virtualization Interfaces by id</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationInterfacesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_interfaces_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationInterfacesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_interfaces_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationInterfacesId(id, callback)</td>
    <td style="padding:15px">virtualization_interfaces_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/interfaces/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationVirtualMachines(id, name, cluster, vcpus, memory, disk, localContextData, tenantGroupId, tenantGroup, tenantId, tenant, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, status, clusterGroupId, clusterGroup, clusterTypeId, clusterType, clusterId, regionId, region, siteId, site, roleId, role, platformId, platform, macAddress, tag, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, clusterN, vcpusN, vcpusLte, vcpusLt, vcpusGte, vcpusGt, memoryN, memoryLte, memoryLt, memoryGte, memoryGt, diskN, diskLte, diskLt, diskGte, diskGt, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, statusN, clusterGroupIdN, clusterGroupN, clusterTypeIdN, clusterTypeN, clusterIdN, regionIdN, regionN, siteIdN, siteN, roleIdN, roleN, platformIdN, platformN, macAddressN, macAddressIc, macAddressNic, macAddressIew, macAddressNiew, macAddressIsw, macAddressNisw, macAddressIe, macAddressNie, tagN, limit, offset, customFields, callback)</td>
    <td style="padding:15px">get Virtualization Virtual Machines</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVirtualizationVirtualMachines(data, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_create</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVirtualizationVirtualMachinesId(id, callback)</td>
    <td style="padding:15px">get Virtualization Virtual Machines by id</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVirtualizationVirtualMachinesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchVirtualizationVirtualMachinesId(id, data, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVirtualizationVirtualMachinesId(id, callback)</td>
    <td style="padding:15px">virtualization_virtual-machines_delete</td>
    <td style="padding:15px">{base_path}/{version}/virtualization/virtual-machines/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGraphql(body, callback)</td>
    <td style="padding:15px">A lightweight read-only endpoint for conveying querying using graphql.</td>
    <td style="padding:15px">/graphql/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimLocations(id, name, slug, description, tenantGroupId, tenantGroup, tenantId, tenant, contact, contactRole, contactGroup, created, createdGte, createdLte, lastUpdated, lastUpdatedGte, lastUpdatedLte, q, tag, regionId, region, siteGroupId, siteGroup, siteId, site, parentId, parent, idN, idLte, idLt, idGte, idGt, nameN, nameIc, nameNic, nameIew, nameNiew, nameIsw, nameNisw, nameIe, nameNie, slugN, slugIc, slugNic, slugIew, slugNiew, slugIsw, slugNisw, slugIe, slugNie, descriptionN, descriptionIc, descriptionNic, descriptionIew, descriptionNiew, descriptionIsw, descriptionNisw, descriptionIe, descriptionNie, tenantGroupIdN, tenantGroupN, tenantIdN, tenantN, contactN, contactRoleN, contactGroupN, tagN, regionIdN, regionN, siteGroupIdN, siteGroupN, siteIdN, siteN, parentIdN, parentN, limit, offset, callback)</td>
    <td style="padding:15px">Overrides ListModelMixin to allow processing ExportTemplates.</td>
    <td style="padding:15px">{base_path}/{version}/dcim/locations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDcimLocations(data, callback)</td>
    <td style="padding:15px">dcim_locations_create</td>
    <td style="padding:15px">{base_path}/{version}/dcim/locations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimLocations(data, callback)</td>
    <td style="padding:15px">dcim_locations_bulk_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/locations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimLocations(data, callback)</td>
    <td style="padding:15px">dcim_locations_bulk_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/locations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimLocations(callback)</td>
    <td style="padding:15px">dcim_locations_bulk_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/locations/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDcimLocationsId(id, callback)</td>
    <td style="padding:15px">dcim_locations_read</td>
    <td style="padding:15px">{base_path}/{version}/dcim/locations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDcimLocationsId(id, data, callback)</td>
    <td style="padding:15px">dcim_locations_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/locations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDcimLocationsId(id, data, callback)</td>
    <td style="padding:15px">dcim_locations_partial_update</td>
    <td style="padding:15px">{base_path}/{version}/dcim/locations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDcimLocationsId(id, callback)</td>
    <td style="padding:15px">dcim_locations_delete</td>
    <td style="padding:15px">{base_path}/{version}/dcim/locations/{pathv1}/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
